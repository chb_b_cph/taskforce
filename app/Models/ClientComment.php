<?php

namespace App\Models;

use Eloquent;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

/**
 * App\Models\ClientComment
 *
 * @property int $client_id client id
 * @property string $value value
 * @property Client $client belongsTo
 * @property int $id
 * @method   static Builder|ClientComment newModelQuery()
 * @method   static Builder|ClientComment newQuery()
 * @method   static Builder|ClientComment query()
 * @method   static Builder|ClientComment whereClientId($value)
 * @method   static Builder|ClientComment whereId($value)
 * @method   static Builder|ClientComment whereValue($value)
 * @mixin    Eloquent
 */
class ClientComment extends Model
{

    /**
     * Database table name
     */
    protected $table = 'client_comments';

    /**
     * Mass assignable columns
     */
    protected $fillable = ['value',
        'client_id',
        'value'];

    /**
     * Date time columns.
     */
    protected $dates = [];

    /**
     * client
     *
     * @return BelongsTo
     */
    public function client()
    {
        return $this->belongsTo(Client::class, 'client_id');
    }
}
