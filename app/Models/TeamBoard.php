<?php

namespace App\Models;

use Eloquent;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;

/**
 * App\Models\TeamBoard
 *
 * @property      string $name name
 * @property      string $description description
 * @property      string $logo_path logo path
 * @property      string $color_code color code
 * @property      int $team_id team id
 * @property      string $type type
 * @property      string $css css
 * @property      string $js js
 * @property      Team $team belongsTo
 * @property      Collection $teamBoardColumn hasMany
 * @property      int $id
 * @property-read Collection|TeamBoardColumn[] $teamBoardColumns
 * @property-read int|null $team_board_columns_count
 * @method        static Builder|TeamBoard newModelQuery()
 * @method        static Builder|TeamBoard newQuery()
 * @method        static Builder|TeamBoard query()
 * @method        static Builder|TeamBoard whereColorCode($value)
 * @method        static Builder|TeamBoard whereCss($value)
 * @method        static Builder|TeamBoard whereDescription($value)
 * @method        static Builder|TeamBoard whereId($value)
 * @method        static Builder|TeamBoard whereJs($value)
 * @method        static Builder|TeamBoard whereLogoPath($value)
 * @method        static Builder|TeamBoard whereName($value)
 * @method        static Builder|TeamBoard whereTeamId($value)
 * @method        static Builder|TeamBoard whereType($value)
 * @mixin         Eloquent
 */
class TeamBoard extends Model
{

    /**
     * Database table name
     */
    protected $table = 'team_boards';

    /**
     * Mass assignable columns
     */
    protected $fillable = ['js',
        'name',
        'description',
        'logo_path',
        'color_code',
        'team_id',
        'type',
        'css',
        'js'];

    /**
     * Date time columns.
     */
    protected $dates = [];

    /**
     * team
     *
     * @return BelongsTo
     */
    public function team()
    {
        return $this->belongsTo(Team::class, 'team_id');
    }

    /**
     * teamBoardColumns
     *
     * @return HasMany
     */
    public function teamBoardColumns()
    {
        return $this->hasMany(TeamBoardColumn::class, 'team_board_id');
    }
}
