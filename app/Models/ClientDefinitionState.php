<?php

namespace App\Models;

use Eloquent;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;

/**
 * App\Models\ClientDefinitionState
 *
 * @property      int $client_definition_id client definition id
 * @property      int $state_type_id state type id
 * @property      int $sort_order sort order
 * @property      string $name name
 * @property      StateType $stateType belongsTo
 * @property      ClientDefinition $clientDefinition belongsTo
 * @property      Collection $client hasMany
 * @property      int $id
 * @property-read Collection|Client[] $clients
 * @property-read int|null $clients_count
 * @method        static Builder|ClientDefinitionState newModelQuery()
 * @method        static Builder|ClientDefinitionState newQuery()
 * @method        static Builder|ClientDefinitionState query()
 * @method        static Builder|ClientDefinitionState whereClientDefinitionId($value)
 * @method        static Builder|ClientDefinitionState whereId($value)
 * @method        static Builder|ClientDefinitionState whereName($value)
 * @method        static Builder|ClientDefinitionState whereSortOrder($value)
 * @method        static Builder|ClientDefinitionState whereStateTypeId($value)
 * @mixin         Eloquent
 */
class ClientDefinitionState extends Model
{

    /**
     * Database table name
     */
    protected $table = 'client_definition_states';

    /**
     * Mass assignable columns
     */
    protected $fillable = ['name',
        'client_definition_id',
        'state_type_id',
        'sort_order',
        'name'];

    /**
     * Date time columns.
     */
    protected $dates = [];

    /**
     * stateType
     *
     * @return BelongsTo
     */
    public function stateType()
    {
        return $this->belongsTo(StateType::class, 'state_type_id');
    }

    /**
     * clientDefinition
     *
     * @return BelongsTo
     */
    public function clientDefinition()
    {
        return $this->belongsTo(ClientDefinition::class, 'client_definition_id');
    }

    /**
     * clients
     *
     * @return HasMany
     */
    public function clients()
    {
        return $this->hasMany(Client::class, 'client_definition_state_id');
    }
}
