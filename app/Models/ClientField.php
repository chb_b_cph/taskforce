<?php

namespace App\Models;

use Eloquent;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

/**
 * App\Models\ClientField
 *
 * @property int $client_id client id
 * @property int $client_definition_field_id client definition field id
 * @property string $value value
 * @property ClientDefinitionField $clientDefinitionField belongsTo
 * @property Client $client belongsTo
 * @property int $id
 * @method   static Builder|ClientField newModelQuery()
 * @method   static Builder|ClientField newQuery()
 * @method   static Builder|ClientField query()
 * @method   static Builder|ClientField whereClientDefinitionFieldId($value)
 * @method   static Builder|ClientField whereClientId($value)
 * @method   static Builder|ClientField whereId($value)
 * @method   static Builder|ClientField whereValue($value)
 * @mixin    Eloquent
 */
class ClientField extends Model
{

    /**
     * Database table name
     */
    protected $table = 'client_fields';

    /**
     * Mass assignable columns
     */
    protected $fillable = ['value',
        'client_id',
        'client_definition_field_id',
        'value'];

    /**
     * Date time columns.
     */
    protected $dates = [];

    /**
     * clientDefinitionField
     *
     * @return BelongsTo
     */
    public function clientDefinitionField()
    {
        return $this->belongsTo(ClientDefinitionField::class, 'client_definition_field_id');
    }

    /**
     * client
     *
     * @return BelongsTo
     */
    public function client()
    {
        return $this->belongsTo(Client::class, 'client_id');
    }
}
