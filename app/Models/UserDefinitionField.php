<?php

namespace App\Models;

use Eloquent;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;

/**
 * App\Models\UserDefinitionField
 *
 * @property      int $user_definition_id user definition id
 * @property      int $field_definition_id field definition id
 * @property      string $label label
 * @property      int $sort_order sort order
 * @property      boolean $active active
 * @property      string $css css
 * @property      string $js js
 * @property      FieldDefinition $fieldDefinition belongsTo
 * @property      UserDefinition $userDefinition belongsTo
 * @property      Collection $userField hasMany
 * @property      int $id
 * @property-read Collection|UserField[] $userFields
 * @property-read int|null $user_fields_count
 * @method        static Builder|UserDefinitionField newModelQuery()
 * @method        static Builder|UserDefinitionField newQuery()
 * @method        static Builder|UserDefinitionField query()
 * @method        static Builder|UserDefinitionField whereActive($value)
 * @method        static Builder|UserDefinitionField whereCss($value)
 * @method        static Builder|UserDefinitionField whereFieldDefinitionId($value)
 * @method        static Builder|UserDefinitionField whereId($value)
 * @method        static Builder|UserDefinitionField whereJs($value)
 * @method        static Builder|UserDefinitionField whereLabel($value)
 * @method        static Builder|UserDefinitionField whereSortOrder($value)
 * @method        static Builder|UserDefinitionField whereUserDefinitionId($value)
 * @mixin         Eloquent
 */
class UserDefinitionField extends Model
{

    /**
     * Database table name
     */
    protected $table = 'user_definition_fields';

    /**
     * Mass assignable columns
     */
    protected $fillable = ['js',
        'user_definition_id',
        'field_definition_id',
        'label',
        'sort_order',
        'active',
        'css',
        'js'];

    /**
     * Date time columns.
     */
    protected $dates = [];

    /**
     * fieldDefinition
     *
     * @return BelongsTo
     */
    public function fieldDefinition()
    {
        return $this->belongsTo(FieldDefinition::class, 'field_definition_id');
    }

    /**
     * userDefinition
     *
     * @return BelongsTo
     */
    public function userDefinition()
    {
        return $this->belongsTo(UserDefinition::class, 'user_definition_id');
    }

    /**
     * userFields
     *
     * @return HasMany
     */
    public function userFields()
    {
        return $this->hasMany(UserField::class, 'user_definition_field_id');
    }
}
