<?php

namespace App\Models;

use Eloquent;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;

/**
 * App\Models\TeamBoardColumn
 *
 * @property      string $name name
 * @property      string $description description
 * @property      boolean $split_column split column
 * @property      int $sort_order sort order
 * @property      int $team_board_id team board id
 * @property      TeamBoard $teamBoard belongsTo
 * @property      Collection $teamBoardColumnTask hasMany
 * @property      int $id
 * @property-read Collection|TeamBoardColumnTask[] $teamBoardColumnTasks
 * @property-read int|null $team_board_column_tasks_count
 * @method        static Builder|TeamBoardColumn newModelQuery()
 * @method        static Builder|TeamBoardColumn newQuery()
 * @method        static Builder|TeamBoardColumn query()
 * @method        static Builder|TeamBoardColumn whereDescription($value)
 * @method        static Builder|TeamBoardColumn whereId($value)
 * @method        static Builder|TeamBoardColumn whereName($value)
 * @method        static Builder|TeamBoardColumn whereSortOrder($value)
 * @method        static Builder|TeamBoardColumn whereSplitColumn($value)
 * @method        static Builder|TeamBoardColumn whereTeamBoardId($value)
 * @mixin         Eloquent
 */
class TeamBoardColumn extends Model
{

    /**
     * Database table name
     */
    protected $table = 'team_board_columns';

    /**
     * Mass assignable columns
     */
    protected $fillable = ['team_board_id',
        'name',
        'description',
        'split_column',
        'sort_order',
        'team_board_id'];

    /**
     * Date time columns.
     */
    protected $dates = [];

    /**
     * teamBoard
     *
     * @return BelongsTo
     */
    public function teamBoard()
    {
        return $this->belongsTo(TeamBoard::class, 'team_board_id');
    }

    /**
     * teamBoardColumnTasks
     *
     * @return HasMany
     */
    public function teamBoardColumnTasks()
    {
        return $this->hasMany(TeamBoardColumnTask::class, 'team_board_column_id');
    }
}
