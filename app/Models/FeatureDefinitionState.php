<?php

namespace App\Models;

use Eloquent;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;

/**
 * App\Models\FeatureDefinitionState
 *
 * @property      int $feature_definition_id feature definition id
 * @property      int $state_type_id state type id
 * @property      int $sort_order sort order
 * @property      string $name name
 * @property      StateType $stateType belongsTo
 * @property      FeatureDefinition $featureDefinition belongsTo
 * @property      Collection $feature hasMany
 * @property      int $id
 * @property-read Collection|Feature[] $features
 * @property-read int|null $features_count
 * @method        static Builder|FeatureDefinitionState newModelQuery()
 * @method        static Builder|FeatureDefinitionState newQuery()
 * @method        static Builder|FeatureDefinitionState query()
 * @method        static Builder|FeatureDefinitionState whereFeatureDefinitionId($value)
 * @method        static Builder|FeatureDefinitionState whereId($value)
 * @method        static Builder|FeatureDefinitionState whereName($value)
 * @method        static Builder|FeatureDefinitionState whereSortOrder($value)
 * @method        static Builder|FeatureDefinitionState whereStateTypeId($value)
 * @mixin         Eloquent
 */
class FeatureDefinitionState extends Model
{

    /**
     * Database table name
     */
    protected $table = 'feature_definition_states';

    /**
     * Mass assignable columns
     */
    protected $fillable = ['name',
        'feature_definition_id',
        'state_type_id',
        'sort_order',
        'name'];

    /**
     * Date time columns.
     */
    protected $dates = [];

    /**
     * stateType
     *
     * @return BelongsTo
     */
    public function stateType()
    {
        return $this->belongsTo(StateType::class, 'state_type_id');
    }

    /**
     * featureDefinition
     *
     * @return BelongsTo
     */
    public function featureDefinition()
    {
        return $this->belongsTo(FeatureDefinition::class, 'feature_definition_id');
    }

    /**
     * features
     *
     * @return HasMany
     */
    public function features()
    {
        return $this->hasMany(Feature::class, 'feature_definition_state_id');
    }
}
