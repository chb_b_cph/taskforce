<?php

namespace App\Models;

use Eloquent;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

/**
 * App\Models\FeatureComment
 *
 * @property int $feature_id feature id
 * @property string $value value
 * @property boolean $client_visible client visible
 * @property Feature $feature belongsTo
 * @property int $id
 * @method   static Builder|FeatureComment newModelQuery()
 * @method   static Builder|FeatureComment newQuery()
 * @method   static Builder|FeatureComment query()
 * @method   static Builder|FeatureComment whereClientVisible($value)
 * @method   static Builder|FeatureComment whereFeatureId($value)
 * @method   static Builder|FeatureComment whereId($value)
 * @method   static Builder|FeatureComment whereValue($value)
 * @mixin    Eloquent
 */
class FeatureComment extends Model
{

    /**
     * Database table name
     */
    protected $table = 'feature_comments';

    /**
     * Mass assignable columns
     */
    protected $fillable = ['client_visible',
        'feature_id',
        'value',
        'client_visible'];

    /**
     * Date time columns.
     */
    protected $dates = [];

    /**
     * feature
     *
     * @return BelongsTo
     */
    public function feature()
    {
        return $this->belongsTo(Feature::class, 'feature_id');
    }
}
