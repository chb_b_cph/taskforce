<?php

namespace App\Models;

use Eloquent;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;

/**
 * App\Models\ProductDefinitionField
 *
 * @property      int $product_definition_id product definition id
 * @property      int $field_definition_id field definition id
 * @property      string $label label
 * @property      int $sort_order sort order
 * @property      boolean $active active
 * @property      string $css css
 * @property      string $js js
 * @property      FieldDefinition $fieldDefinition belongsTo
 * @property      ProductDefinition $productDefinition belongsTo
 * @property      Collection $productField hasMany
 * @property      int $id
 * @property-read Collection|ProductField[] $productFields
 * @property-read int|null $product_fields_count
 * @method        static Builder|ProductDefinitionField newModelQuery()
 * @method        static Builder|ProductDefinitionField newQuery()
 * @method        static Builder|ProductDefinitionField query()
 * @method        static Builder|ProductDefinitionField whereActive($value)
 * @method        static Builder|ProductDefinitionField whereCss($value)
 * @method        static Builder|ProductDefinitionField whereFieldDefinitionId($value)
 * @method        static Builder|ProductDefinitionField whereId($value)
 * @method        static Builder|ProductDefinitionField whereJs($value)
 * @method        static Builder|ProductDefinitionField whereLabel($value)
 * @method        static Builder|ProductDefinitionField whereProductDefinitionId($value)
 * @method        static Builder|ProductDefinitionField whereSortOrder($value)
 * @mixin         Eloquent
 */
class ProductDefinitionField extends Model
{

    /**
     * Database table name
     */
    protected $table = 'product_definition_fields';

    /**
     * Mass assignable columns
     */
    protected $fillable = ['js',
        'product_definition_id',
        'field_definition_id',
        'label',
        'sort_order',
        'active',
        'css',
        'js'];

    /**
     * Date time columns.
     */
    protected $dates = [];

    /**
     * fieldDefinition
     *
     * @return BelongsTo
     */
    public function fieldDefinition()
    {
        return $this->belongsTo(FieldDefinition::class, 'field_definition_id');
    }

    /**
     * productDefinition
     *
     * @return BelongsTo
     */
    public function productDefinition()
    {
        return $this->belongsTo(ProductDefinition::class, 'product_definition_id');
    }

    /**
     * productFields
     *
     * @return HasMany
     */
    public function productFields()
    {
        return $this->hasMany(ProductField::class, 'product_definition_field_id');
    }
}
