<?php

namespace App\Models;

use Eloquent;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

/**
 * App\Models\TaskDocument
 *
 * @property int $task_id task id
 * @property string $file_name file name
 * @property bigint $file_size file size
 * @property string $mime_type mime type
 * @property blob $file_data file data
 * @property string $description description
 * @property Task $task belongsTo
 * @property int $id
 * @method   static Builder|TaskDocument newModelQuery()
 * @method   static Builder|TaskDocument newQuery()
 * @method   static Builder|TaskDocument query()
 * @method   static Builder|TaskDocument whereDescription($value)
 * @method   static Builder|TaskDocument whereFileData($value)
 * @method   static Builder|TaskDocument whereFileName($value)
 * @method   static Builder|TaskDocument whereFileSize($value)
 * @method   static Builder|TaskDocument whereId($value)
 * @method   static Builder|TaskDocument whereMimeType($value)
 * @method   static Builder|TaskDocument whereTaskId($value)
 * @mixin    Eloquent
 */
class TaskDocument extends Model
{

    /**
     * Database table name
     */
    protected $table = 'task_documents';

    /**
     * Mass assignable columns
     */
    protected $fillable = ['description',
        'task_id',
        'file_name',
        'file_size',
        'mime_type',
        'file_data',
        'description'];

    /**
     * Date time columns.
     */
    protected $dates = [];

    /**
     * task
     *
     * @return BelongsTo
     */
    public function task()
    {
        return $this->belongsTo(Task::class, 'task_id');
    }
}
