<?php

namespace App\Models;

use Eloquent;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

/**
 * App\Models\TaskField
 *
 * @property int $task_id task id
 * @property int $task_definition_field_id task definition field id
 * @property string $value value
 * @property TaskDefinitionField $taskDefinitionField belongsTo
 * @property Task $task belongsTo
 * @property int $id
 * @method   static Builder|TaskField newModelQuery()
 * @method   static Builder|TaskField newQuery()
 * @method   static Builder|TaskField query()
 * @method   static Builder|TaskField whereId($value)
 * @method   static Builder|TaskField whereTaskDefinitionFieldId($value)
 * @method   static Builder|TaskField whereTaskId($value)
 * @method   static Builder|TaskField whereValue($value)
 * @mixin    Eloquent
 */
class TaskField extends Model
{

    /**
     * Database table name
     */
    protected $table = 'task_fields';

    /**
     * Mass assignable columns
     */
    protected $fillable = ['value',
        'task_id',
        'task_definition_field_id',
        'value'];

    /**
     * Date time columns.
     */
    protected $dates = [];

    /**
     * taskDefinitionField
     *
     * @return BelongsTo
     */
    public function taskDefinitionField()
    {
        return $this->belongsTo(TaskDefinitionField::class, 'task_definition_field_id');
    }

    /**
     * task
     *
     * @return BelongsTo
     */
    public function task()
    {
        return $this->belongsTo(Task::class, 'task_id');
    }
}
