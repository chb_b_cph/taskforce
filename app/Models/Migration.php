<?php

namespace App\Models;

use Eloquent;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\Migration
 *
 * @property string $migration migration
 * @property int $batch batch
 * @property int $id
 * @method   static Builder|Migration newModelQuery()
 * @method   static Builder|Migration newQuery()
 * @method   static Builder|Migration query()
 * @method   static Builder|Migration whereBatch($value)
 * @method   static Builder|Migration whereId($value)
 * @method   static Builder|Migration whereMigration($value)
 * @mixin    Eloquent
 */
class Migration extends Model
{

    /**
     * Database table name
     */
    protected $table = 'migrations';

    /**
     * Mass assignable columns
     */
    protected $fillable = ['batch',
        'migration',
        'batch'];

    /**
     * Date time columns.
     */
    protected $dates = [];
}
