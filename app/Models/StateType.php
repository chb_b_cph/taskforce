<?php

namespace App\Models;

use Eloquent;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;

/**
 * App\Models\StateType
 *
 * @property      string $name name
 * @property      string $description description
 * @property      string $data_type data type
 * @property      int $default_length default length
 * @property      int $default_precision default precision
 * @property      Collection $clientDefinitionState hasMany
 * @property      Collection $featureDefinitionState hasMany
 * @property      Collection $productDefinitionState hasMany
 * @property      Collection $taskDefinitionState hasMany
 * @property      int $id
 * @property-read Collection|ClientDefinitionState[] $clientDefinitionStates
 * @property-read int|null $client_definition_states_count
 * @property-read Collection|FeatureDefinitionState[] $featureDefinitionStates
 * @property-read int|null $feature_definition_states_count
 * @property-read Collection|ProductDefinitionState[] $productDefinitionStates
 * @property-read int|null $product_definition_states_count
 * @property-read Collection|TaskDefinitionState[] $taskDefinitionStates
 * @property-read int|null $task_definition_states_count
 * @method        static Builder|StateType newModelQuery()
 * @method        static Builder|StateType newQuery()
 * @method        static Builder|StateType query()
 * @method        static Builder|StateType whereDataType($value)
 * @method        static Builder|StateType whereDefaultLength($value)
 * @method        static Builder|StateType whereDefaultPrecision($value)
 * @method        static Builder|StateType whereDescription($value)
 * @method        static Builder|StateType whereId($value)
 * @method        static Builder|StateType whereName($value)
 * @mixin         Eloquent
 */
class StateType extends Model
{

    /**
     * Database table name
     */
    protected $table = 'state_types';

    /**
     * Mass assignable columns
     */
    protected $fillable = ['default_precision',
        'name',
        'description',
        'data_type',
        'default_length',
        'default_precision'];

    /**
     * Date time columns.
     */
    protected $dates = [];

    /**
     * clientDefinitionStates
     *
     * @return HasMany
     */
    public function clientDefinitionStates()
    {
        return $this->hasMany(ClientDefinitionState::class, 'state_type_id');
    }

    /**
     * featureDefinitionStates
     *
     * @return HasMany
     */
    public function featureDefinitionStates()
    {
        return $this->hasMany(FeatureDefinitionState::class, 'state_type_id');
    }

    /**
     * productDefinitionStates
     *
     * @return HasMany
     */
    public function productDefinitionStates()
    {
        return $this->hasMany(ProductDefinitionState::class, 'state_type_id');
    }

    /**
     * taskDefinitionStates
     *
     * @return HasMany
     */
    public function taskDefinitionStates()
    {
        return $this->hasMany(TaskDefinitionState::class, 'state_type_id');
    }
}
