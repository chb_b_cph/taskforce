<?php

namespace App\Models;

use Eloquent;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

/**
 * App\Models\FeatureField
 *
 * @property int $feature_id feature id
 * @property int $feature_definition_field_id feature definition field id
 * @property string $value value
 * @property FeatureDefinitionField $featureDefinitionField belongsTo
 * @property Feature $feature belongsTo
 * @property int $id
 * @method   static Builder|FeatureField newModelQuery()
 * @method   static Builder|FeatureField newQuery()
 * @method   static Builder|FeatureField query()
 * @method   static Builder|FeatureField whereFeatureDefinitionFieldId($value)
 * @method   static Builder|FeatureField whereFeatureId($value)
 * @method   static Builder|FeatureField whereId($value)
 * @method   static Builder|FeatureField whereValue($value)
 * @mixin    Eloquent
 */
class FeatureField extends Model
{

    /**
     * Database table name
     */
    protected $table = 'feature_fields';

    /**
     * Mass assignable columns
     */
    protected $fillable = ['value',
        'feature_id',
        'feature_definition_field_id',
        'value'];

    /**
     * Date time columns.
     */
    protected $dates = [];

    /**
     * featureDefinitionField
     *
     * @return BelongsTo
     */
    public function featureDefinitionField()
    {
        return $this->belongsTo(FeatureDefinitionField::class, 'feature_definition_field_id');
    }

    /**
     * feature
     *
     * @return BelongsTo
     */
    public function feature()
    {
        return $this->belongsTo(Feature::class, 'feature_id');
    }
}
