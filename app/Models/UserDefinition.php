<?php

namespace App\Models;

use Eloquent;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;

/**
 * App\Models\UserDefinition
 *
 * @property      string $name name
 * @property      string $description description
 * @property      boolean $active active
 * @property      Collection $userDefinitionField hasMany
 * @property      Collection $user hasMany
 * @property      int $id
 * @property-read Collection|UserDefinitionField[] $userDefinitionFields
 * @property-read int|null $user_definition_fields_count
 * @property-read Collection|User[] $users
 * @property-read int|null $users_count
 * @method        static Builder|UserDefinition newModelQuery()
 * @method        static Builder|UserDefinition newQuery()
 * @method        static Builder|UserDefinition query()
 * @method        static Builder|UserDefinition whereActive($value)
 * @method        static Builder|UserDefinition whereDescription($value)
 * @method        static Builder|UserDefinition whereId($value)
 * @method        static Builder|UserDefinition whereName($value)
 * @mixin         Eloquent
 */
class UserDefinition extends Model
{

    /**
     * Database table name
     */
    protected $table = 'user_definitions';

    /**
     * Mass assignable columns
     */
    protected $fillable = ['active',
        'name',
        'description',
        'active'];

    /**
     * Date time columns.
     */
    protected $dates = [];

    /**
     * userDefinitionFields
     *
     * @return HasMany
     */
    public function userDefinitionFields()
    {
        return $this->hasMany(UserDefinitionField::class, 'user_definition_id');
    }

    /**
     * users
     *
     * @return HasMany
     */
    public function users()
    {
        return $this->hasMany(User::class, 'user_definition_id');
    }
}
