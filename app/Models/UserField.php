<?php

namespace App\Models;

use Eloquent;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

/**
 * App\Models\UserField
 *
 * @property int $user_id user id
 * @property int $user_definition_field_id user definition field id
 * @property string $value value
 * @property UserDefinitionField $userDefinitionField belongsTo
 * @property User $user belongsTo
 * @property int $id
 * @method   static Builder|UserField newModelQuery()
 * @method   static Builder|UserField newQuery()
 * @method   static Builder|UserField query()
 * @method   static Builder|UserField whereId($value)
 * @method   static Builder|UserField whereUserDefinitionFieldId($value)
 * @method   static Builder|UserField whereUserId($value)
 * @method   static Builder|UserField whereValue($value)
 * @mixin    Eloquent
 */
class UserField extends Model
{

    /**
     * Database table name
     */
    protected $table = 'user_fields';

    /**
     * Mass assignable columns
     */
    protected $fillable = ['value',
        'user_id',
        'user_definition_field_id',
        'value'];

    /**
     * Date time columns.
     */
    protected $dates = [];

    /**
     * userDefinitionField
     *
     * @return BelongsTo
     */
    public function userDefinitionField()
    {
        return $this->belongsTo(UserDefinitionField::class, 'user_definition_field_id');
    }

    /**
     * user
     *
     * @return BelongsTo
     */
    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }
}
