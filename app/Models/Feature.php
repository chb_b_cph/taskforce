<?php

namespace App\Models;

use Eloquent;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;

/**
 * App\Models\Feature
 *
 * @property      int $product_id product id
 * @property      int $feature_definition_id feature definition id
 * @property      int $feature_definition_state_id feature definition state id
 * @property      string $name name
 * @property      string $description description
 * @property      boolean $client_visible client visible
 * @property      FeatureDefinitionState $featureDefinitionState belongsTo
 * @property      FeatureDefinition $featureDefinition belongsTo
 * @property      Product $product belongsTo
 * @property      Collection $featureComment hasMany
 * @property      Collection $featureDocument hasMany
 * @property      Collection $featureField hasMany
 * @property      Collection $task hasMany
 * @property      int $id
 * @property-read Collection|FeatureComment[] $featureComments
 * @property-read int|null $feature_comments_count
 * @property-read Collection|FeatureDocument[] $featureDocuments
 * @property-read int|null $feature_documents_count
 * @property-read Collection|FeatureField[] $featureFields
 * @property-read int|null $feature_fields_count
 * @property-read Collection|Task[] $tasks
 * @property-read int|null $tasks_count
 * @method        static Builder|Feature newModelQuery()
 * @method        static Builder|Feature newQuery()
 * @method        static Builder|Feature query()
 * @method        static Builder|Feature whereClientVisible($value)
 * @method        static Builder|Feature whereDescription($value)
 * @method        static Builder|Feature whereFeatureDefinitionId($value)
 * @method        static Builder|Feature whereFeatureDefinitionStateId($value)
 * @method        static Builder|Feature whereId($value)
 * @method        static Builder|Feature whereName($value)
 * @method        static Builder|Feature whereProductId($value)
 * @mixin         Eloquent
 */
class Feature extends Model
{

    /**
     * Database table name
     */
    protected $table = 'features';

    /**
     * Mass assignable columns
     */
    protected $fillable = ['client_visible',
        'product_id',
        'feature_definition_id',
        'feature_definition_state_id',
        'name',
        'description',
        'client_visible'];

    /**
     * Date time columns.
     */
    protected $dates = [];

    /**
     * featureDefinitionState
     *
     * @return BelongsTo
     */
    public function featureDefinitionState()
    {
        return $this->belongsTo(FeatureDefinitionState::class, 'feature_definition_state_id');
    }

    /**
     * featureDefinition
     *
     * @return BelongsTo
     */
    public function featureDefinition()
    {
        return $this->belongsTo(FeatureDefinition::class, 'feature_definition_id');
    }

    /**
     * product
     *
     * @return BelongsTo
     */
    public function product()
    {
        return $this->belongsTo(Product::class, 'product_id');
    }

    /**
     * featureComments
     *
     * @return HasMany
     */
    public function featureComments()
    {
        return $this->hasMany(FeatureComment::class, 'feature_id');
    }

    /**
     * featureDocuments
     *
     * @return HasMany
     */
    public function featureDocuments()
    {
        return $this->hasMany(FeatureDocument::class, 'feature_id');
    }

    /**
     * featureFields
     *
     * @return HasMany
     */
    public function featureFields()
    {
        return $this->hasMany(FeatureField::class, 'feature_id');
    }

    /**
     * tasks
     *
     * @return HasMany
     */
    public function tasks()
    {
        return $this->hasMany(Task::class, 'feature_id');
    }
}
