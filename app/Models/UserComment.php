<?php

namespace App\Models;

use Eloquent;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

/**
 * App\Models\UserComment
 *
 * @property int $user_id user id
 * @property string $value value
 * @property User $user belongsTo
 * @property int $id
 * @method   static Builder|UserComment newModelQuery()
 * @method   static Builder|UserComment newQuery()
 * @method   static Builder|UserComment query()
 * @method   static Builder|UserComment whereId($value)
 * @method   static Builder|UserComment whereUserId($value)
 * @method   static Builder|UserComment whereValue($value)
 * @mixin    Eloquent
 */
class UserComment extends Model
{

    /**
     * Database table name
     */
    protected $table = 'user_comments';

    /**
     * Mass assignable columns
     */
    protected $fillable = ['value',
        'user_id',
        'value'];

    /**
     * Date time columns.
     */
    protected $dates = [];

    /**
     * user
     *
     * @return BelongsTo
     */
    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }
}
