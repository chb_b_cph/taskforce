<?php

namespace App\Models;

use Eloquent;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

/**
 * App\Models\ProductComment
 *
 * @property int $product_id product id
 * @property string $value value
 * @property Product $product belongsTo
 * @property int $id
 * @method   static Builder|ProductComment newModelQuery()
 * @method   static Builder|ProductComment newQuery()
 * @method   static Builder|ProductComment query()
 * @method   static Builder|ProductComment whereId($value)
 * @method   static Builder|ProductComment whereProductId($value)
 * @method   static Builder|ProductComment whereValue($value)
 * @mixin    Eloquent
 */
class ProductComment extends Model
{

    /**
     * Database table name
     */
    protected $table = 'product_comments';

    /**
     * Mass assignable columns
     */
    protected $fillable = ['value',
        'product_id',
        'value'];

    /**
     * Date time columns.
     */
    protected $dates = [];

    /**
     * product
     *
     * @return BelongsTo
     */
    public function product()
    {
        return $this->belongsTo(Product::class, 'product_id');
    }
}
