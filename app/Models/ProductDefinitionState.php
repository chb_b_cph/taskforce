<?php

namespace App\Models;

use Eloquent;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;

/**
 * App\Models\ProductDefinitionState
 *
 * @property      int $product_definition_id product definition id
 * @property      int $state_type_id state type id
 * @property      int $sort_order sort order
 * @property      string $name name
 * @property      StateType $stateType belongsTo
 * @property      ProductDefinition $productDefinition belongsTo
 * @property      Collection $product hasMany
 * @property      int $id
 * @property-read Collection|Product[] $products
 * @property-read int|null $products_count
 * @method        static Builder|ProductDefinitionState newModelQuery()
 * @method        static Builder|ProductDefinitionState newQuery()
 * @method        static Builder|ProductDefinitionState query()
 * @method        static Builder|ProductDefinitionState whereId($value)
 * @method        static Builder|ProductDefinitionState whereName($value)
 * @method        static Builder|ProductDefinitionState whereProductDefinitionId($value)
 * @method        static Builder|ProductDefinitionState whereSortOrder($value)
 * @method        static Builder|ProductDefinitionState whereStateTypeId($value)
 * @mixin         Eloquent
 */
class ProductDefinitionState extends Model
{

    /**
     * Database table name
     */
    protected $table = 'product_definition_states';

    /**
     * Mass assignable columns
     */
    protected $fillable = ['name',
        'product_definition_id',
        'state_type_id',
        'sort_order',
        'name'];

    /**
     * Date time columns.
     */
    protected $dates = [];

    /**
     * stateType
     *
     * @return BelongsTo
     */
    public function stateType()
    {
        return $this->belongsTo(StateType::class, 'state_type_id');
    }

    /**
     * productDefinition
     *
     * @return BelongsTo
     */
    public function productDefinition()
    {
        return $this->belongsTo(ProductDefinition::class, 'product_definition_id');
    }

    /**
     * products
     *
     * @return HasMany
     */
    public function products()
    {
        return $this->hasMany(Product::class, 'product_definition_state_id');
    }
}
