<?php

namespace App\Models;

use Eloquent;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;

/**
 * App\Models\User
 *
 * @property      int $user_definition_id user definition id
 * @property      string $first_name first name
 * @property      string $last_name last name
 * @property      string $email email
 * @property      string $password password
 * @property      boolean $active active
 * @property      UserDefinition $userDefinition belongsTo
 * @property      Collection $clientUser hasMany
 * @property      Collection $teamUser hasMany
 * @property      Collection $userComment hasMany
 * @property      Collection $userField hasMany
 * @property      int $id
 * @property-read Collection|ClientUser[] $clientUsers
 * @property-read int|null $client_users_count
 * @property-read Collection|TeamUser[] $teamUsers
 * @property-read int|null $team_users_count
 * @property-read Collection|UserComment[] $userComments
 * @property-read int|null $user_comments_count
 * @property-read Collection|UserField[] $userFields
 * @property-read int|null $user_fields_count
 * @method        static Builder|User newModelQuery()
 * @method        static Builder|User newQuery()
 * @method        static Builder|User query()
 * @method        static Builder|User whereActive($value)
 * @method        static Builder|User whereEmail($value)
 * @method        static Builder|User whereFirstName($value)
 * @method        static Builder|User whereId($value)
 * @method        static Builder|User whereLastName($value)
 * @method        static Builder|User wherePassword($value)
 * @method        static Builder|User whereUserDefinitionId($value)
 * @mixin         Eloquent
 */
class User extends Model
{

    /**
     * Database table name
     */
    protected $table = 'users';

    /**
     * Mass assignable columns
     */
    protected $fillable = ['active',
        'user_definition_id',
        'first_name',
        'last_name',
        'email',
        'active'];

    /**
     * Date time columns.
     */
    protected $dates = [];

    /**
     * userDefinition
     *
     * @return BelongsTo
     */
    public function userDefinition()
    {
        return $this->belongsTo(UserDefinition::class, 'user_definition_id');
    }

    /**
     * clientUsers
     *
     * @return HasMany
     */
    public function clientUsers()
    {
        return $this->hasMany(ClientUser::class, 'user_id');
    }

    /**
     * teamUsers
     *
     * @return HasMany
     */
    public function teamUsers()
    {
        return $this->hasMany(TeamUser::class, 'user_id');
    }

    /**
     * userComments
     *
     * @return HasMany
     */
    public function userComments()
    {
        return $this->hasMany(UserComment::class, 'user_id');
    }

    /**
     * userFields
     *
     * @return HasMany
     */
    public function userFields()
    {
        return $this->hasMany(UserField::class, 'user_id');
    }
}
