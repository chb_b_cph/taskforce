<?php

namespace App\Models;

use Eloquent;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;

/**
 * App\Models\Client
 *
 * @property      int $client_definition_id client definition id
 * @property      int $client_definition_state_id client definition state id
 * @property      string $name name
 * @property      string $description description
 * @property      ClientDefinitionState $clientDefinitionState belongsTo
 * @property      ClientDefinition $clientDefinition belongsTo
 * @property      Collection $clientComment hasMany
 * @property      Collection $clientDocument hasMany
 * @property      Collection $clientField hasMany
 * @property      Collection $clientUser hasMany
 * @property      Collection $product hasMany
 * @property      int $id
 * @property-read Collection|ClientComment[] $clientComments
 * @property-read int|null $client_comments_count
 * @property-read Collection|ClientDocument[] $clientDocuments
 * @property-read int|null $client_documents_count
 * @property-read Collection|ClientField[] $clientFields
 * @property-read int|null $client_fields_count
 * @property-read Collection|ClientUser[] $clientUsers
 * @property-read int|null $client_users_count
 * @property-read Collection|Product[] $products
 * @property-read int|null $products_count
 * @method        static Builder|Client newModelQuery()
 * @method        static Builder|Client newQuery()
 * @method        static Builder|Client query()
 * @method        static Builder|Client whereClientDefinitionId($value)
 * @method        static Builder|Client whereClientDefinitionStateId($value)
 * @method        static Builder|Client whereDescription($value)
 * @method        static Builder|Client whereId($value)
 * @method        static Builder|Client whereName($value)
 * @mixin         Eloquent
 */
class Client extends Model
{

    /**
     * Database table name
     */
    protected $table = 'clients';

    /**
     * Mass assignable columns
     */
    protected $fillable = ['description',
        'client_definition_id',
        'client_definition_state_id',
        'name',
        'description'];

    /**
     * Date time columns.
     */
    protected $dates = [];

    /**
     * clientDefinitionState
     *
     * @return BelongsTo
     */
    public function clientDefinitionState()
    {
        return $this->belongsTo(ClientDefinitionState::class, 'client_definition_state_id');
    }

    /**
     * clientDefinition
     *
     * @return BelongsTo
     */
    public function clientDefinition()
    {
        return $this->belongsTo(ClientDefinition::class, 'client_definition_id');
    }

    /**
     * clientComments
     *
     * @return HasMany
     */
    public function clientComments()
    {
        return $this->hasMany(ClientComment::class, 'client_id');
    }

    /**
     * clientDocuments
     *
     * @return HasMany
     */
    public function clientDocuments()
    {
        return $this->hasMany(ClientDocument::class, 'client_id');
    }

    /**
     * clientFields
     *
     * @return HasMany
     */
    public function clientFields()
    {
        return $this->hasMany(ClientField::class, 'client_id');
    }

    /**
     * clientUsers
     *
     * @return HasMany
     */
    public function clientUsers()
    {
        return $this->hasMany(ClientUser::class, 'client_id');
    }

    /**
     * products
     *
     * @return HasMany
     */
    public function products()
    {
        return $this->hasMany(Product::class, 'client_id');
    }
}
