<?php

namespace App\Models;

use Eloquent;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

/**
 * App\Models\ProductDocument
 *
 * @property int $product_id product id
 * @property string $file_name file name
 * @property bigint $file_size file size
 * @property string $mime_type mime type
 * @property blob $file_data file data
 * @property string $description description
 * @property Product $product belongsTo
 * @property int $id
 * @method   static Builder|ProductDocument newModelQuery()
 * @method   static Builder|ProductDocument newQuery()
 * @method   static Builder|ProductDocument query()
 * @method   static Builder|ProductDocument whereDescription($value)
 * @method   static Builder|ProductDocument whereFileData($value)
 * @method   static Builder|ProductDocument whereFileName($value)
 * @method   static Builder|ProductDocument whereFileSize($value)
 * @method   static Builder|ProductDocument whereId($value)
 * @method   static Builder|ProductDocument whereMimeType($value)
 * @method   static Builder|ProductDocument whereProductId($value)
 * @mixin    Eloquent
 */
class ProductDocument extends Model
{

    /**
     * Database table name
     */
    protected $table = 'product_documents';

    /**
     * Mass assignable columns
     */
    protected $fillable = ['description',
        'product_id',
        'file_name',
        'file_size',
        'mime_type',
        'file_data',
        'description'];

    /**
     * Date time columns.
     */
    protected $dates = [];

    /**
     * product
     *
     * @return BelongsTo
     */
    public function product()
    {
        return $this->belongsTo(Product::class, 'product_id');
    }
}
