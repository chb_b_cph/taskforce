<?php

namespace App\Models;

use Eloquent;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

/**
 * App\Models\FeatureDocument
 *
 * @property int $feature_id feature id
 * @property string $file_name file name
 * @property bigint $file_size file size
 * @property string $mime_type mime type
 * @property blob $file_data file data
 * @property string $description description
 * @property Feature $feature belongsTo
 * @property int $id
 * @method   static Builder|FeatureDocument newModelQuery()
 * @method   static Builder|FeatureDocument newQuery()
 * @method   static Builder|FeatureDocument query()
 * @method   static Builder|FeatureDocument whereDescription($value)
 * @method   static Builder|FeatureDocument whereFeatureId($value)
 * @method   static Builder|FeatureDocument whereFileData($value)
 * @method   static Builder|FeatureDocument whereFileName($value)
 * @method   static Builder|FeatureDocument whereFileSize($value)
 * @method   static Builder|FeatureDocument whereId($value)
 * @method   static Builder|FeatureDocument whereMimeType($value)
 * @mixin    Eloquent
 */
class FeatureDocument extends Model
{

    /**
     * Database table name
     */
    protected $table = 'feature_documents';

    /**
     * Mass assignable columns
     */
    protected $fillable = ['description',
        'feature_id',
        'file_name',
        'file_size',
        'mime_type',
        'file_data',
        'description'];

    /**
     * Date time columns.
     */
    protected $dates = [];

    /**
     * feature
     *
     * @return BelongsTo
     */
    public function feature()
    {
        return $this->belongsTo(Feature::class, 'feature_id');
    }
}
