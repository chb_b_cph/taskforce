<?php

namespace App\Models;

use Eloquent;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;

/**
 * App\Models\TaskDefinitionField
 *
 * @property      int $task_definition_id task definition id
 * @property      int $field_definition_id field definition id
 * @property      string $label label
 * @property      int $sort_order sort order
 * @property      boolean $active active
 * @property      string $css css
 * @property      string $js js
 * @property      boolean $client_updatable client updatable
 * @property      FieldDefinition $fieldDefinition belongsTo
 * @property      TaskDefinition $taskDefinition belongsTo
 * @property      Collection $taskField hasMany
 * @property      int $id
 * @property-read Collection|TaskField[] $taskFields
 * @property-read int|null $task_fields_count
 * @method        static Builder|TaskDefinitionField newModelQuery()
 * @method        static Builder|TaskDefinitionField newQuery()
 * @method        static Builder|TaskDefinitionField query()
 * @method        static Builder|TaskDefinitionField whereActive($value)
 * @method        static Builder|TaskDefinitionField whereClientUpdatable($value)
 * @method        static Builder|TaskDefinitionField whereCss($value)
 * @method        static Builder|TaskDefinitionField whereFieldDefinitionId($value)
 * @method        static Builder|TaskDefinitionField whereId($value)
 * @method        static Builder|TaskDefinitionField whereJs($value)
 * @method        static Builder|TaskDefinitionField whereLabel($value)
 * @method        static Builder|TaskDefinitionField whereSortOrder($value)
 * @method        static Builder|TaskDefinitionField whereTaskDefinitionId($value)
 * @mixin         Eloquent
 */
class TaskDefinitionField extends Model
{

    /**
     * Database table name
     */
    protected $table = 'task_definition_fields';

    /**
     * Mass assignable columns
     */
    protected $fillable = ['client_updatable',
        'task_definition_id',
        'field_definition_id',
        'label',
        'sort_order',
        'active',
        'css',
        'js',
        'client_updatable'];

    /**
     * Date time columns.
     */
    protected $dates = [];

    /**
     * fieldDefinition
     *
     * @return BelongsTo
     */
    public function fieldDefinition()
    {
        return $this->belongsTo(FieldDefinition::class, 'field_definition_id');
    }

    /**
     * taskDefinition
     *
     * @return BelongsTo
     */
    public function taskDefinition()
    {
        return $this->belongsTo(TaskDefinition::class, 'task_definition_id');
    }

    /**
     * taskFields
     *
     * @return HasMany
     */
    public function taskFields()
    {
        return $this->hasMany(TaskField::class, 'task_definition_field_id');
    }
}
