<?php

namespace App\Models;

use Eloquent;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;

/**
 * App\Models\FieldType
 *
 * @property      string $name name
 * @property      string $description description
 * @property      string $data_type data type
 * @property      int $default_length default length
 * @property      int $default_precision default precision
 * @property      Collection $fieldDefinition hasMany
 * @property      int $id
 * @property-read Collection|FieldDefinition[] $fieldDefinitions
 * @property-read int|null $field_definitions_count
 * @method        static Builder|FieldType newModelQuery()
 * @method        static Builder|FieldType newQuery()
 * @method        static Builder|FieldType query()
 * @method        static Builder|FieldType whereDataType($value)
 * @method        static Builder|FieldType whereDefaultLength($value)
 * @method        static Builder|FieldType whereDefaultPrecision($value)
 * @method        static Builder|FieldType whereDescription($value)
 * @method        static Builder|FieldType whereId($value)
 * @method        static Builder|FieldType whereName($value)
 * @mixin         Eloquent
 */
class FieldType extends Model
{

    /**
     * Database table name
     */
    protected $table = 'field_types';

    /**
     * Mass assignable columns
     */
    protected $fillable = ['default_precision',
        'name',
        'description',
        'data_type',
        'default_length',
        'default_precision'];

    /**
     * Date time columns.
     */
    protected $dates = [];

    /**
     * fieldDefinitions
     *
     * @return HasMany
     */
    public function fieldDefinitions()
    {
        return $this->hasMany(FieldDefinition::class, 'field_type_id');
    }
}
