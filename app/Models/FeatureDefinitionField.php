<?php

namespace App\Models;

use Eloquent;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;

/**
 * App\Models\FeatureDefinitionField
 *
 * @property      int $feature_definition_id feature definition id
 * @property      int $field_definition_id field definition id
 * @property      string $label label
 * @property      int $sort_order sort order
 * @property      boolean $active active
 * @property      string $css css
 * @property      string $js js
 * @property      boolean $client_updatable client updatable
 * @property      FieldDefinition $fieldDefinition belongsTo
 * @property      FeatureDefinition $featureDefinition belongsTo
 * @property      Collection $featureField hasMany
 * @property      int $id
 * @property-read Collection|FeatureField[] $featureFields
 * @property-read int|null $feature_fields_count
 * @method        static Builder|FeatureDefinitionField newModelQuery()
 * @method        static Builder|FeatureDefinitionField newQuery()
 * @method        static Builder|FeatureDefinitionField query()
 * @method        static Builder|FeatureDefinitionField whereActive($value)
 * @method        static Builder|FeatureDefinitionField whereClientUpdatable($value)
 * @method        static Builder|FeatureDefinitionField whereCss($value)
 * @method        static Builder|FeatureDefinitionField whereFeatureDefinitionId($value)
 * @method        static Builder|FeatureDefinitionField whereFieldDefinitionId($value)
 * @method        static Builder|FeatureDefinitionField whereId($value)
 * @method        static Builder|FeatureDefinitionField whereJs($value)
 * @method        static Builder|FeatureDefinitionField whereLabel($value)
 * @method        static Builder|FeatureDefinitionField whereSortOrder($value)
 * @mixin         Eloquent
 */
class FeatureDefinitionField extends Model
{

    /**
     * Database table name
     */
    protected $table = 'feature_definition_fields';

    /**
     * Mass assignable columns
     */
    protected $fillable = ['client_updatable',
        'feature_definition_id',
        'field_definition_id',
        'label',
        'sort_order',
        'active',
        'css',
        'js',
        'client_updatable'];

    /**
     * Date time columns.
     */
    protected $dates = [];

    /**
     * fieldDefinition
     *
     * @return BelongsTo
     */
    public function fieldDefinition()
    {
        return $this->belongsTo(FieldDefinition::class, 'field_definition_id');
    }

    /**
     * featureDefinition
     *
     * @return BelongsTo
     */
    public function featureDefinition()
    {
        return $this->belongsTo(FeatureDefinition::class, 'feature_definition_id');
    }

    /**
     * featureFields
     *
     * @return HasMany
     */
    public function featureFields()
    {
        return $this->hasMany(FeatureField::class, 'feature_definition_field_id');
    }
}
