<?php

namespace App\Models;

use Eloquent;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;

/**
 * App\Models\TaskDefinitionState
 *
 * @property      int $task_definition_id task definition id
 * @property      int $state_type_id state type id
 * @property      int $sort_order sort order
 * @property      string $name name
 * @property      StateType $stateType belongsTo
 * @property      TaskDefinition $taskDefinition belongsTo
 * @property      Collection $task hasMany
 * @property      int $id
 * @property-read Collection|Task[] $tasks
 * @property-read int|null $tasks_count
 * @method        static Builder|TaskDefinitionState newModelQuery()
 * @method        static Builder|TaskDefinitionState newQuery()
 * @method        static Builder|TaskDefinitionState query()
 * @method        static Builder|TaskDefinitionState whereId($value)
 * @method        static Builder|TaskDefinitionState whereName($value)
 * @method        static Builder|TaskDefinitionState whereSortOrder($value)
 * @method        static Builder|TaskDefinitionState whereStateTypeId($value)
 * @method        static Builder|TaskDefinitionState whereTaskDefinitionId($value)
 * @mixin         Eloquent
 */
class TaskDefinitionState extends Model
{

    /**
     * Database table name
     */
    protected $table = 'task_definition_states';

    /**
     * Mass assignable columns
     */
    protected $fillable = ['name',
        'task_definition_id',
        'state_type_id',
        'sort_order',
        'name'];

    /**
     * Date time columns.
     */
    protected $dates = [];

    /**
     * stateType
     *
     * @return BelongsTo
     */
    public function stateType()
    {
        return $this->belongsTo(StateType::class, 'state_type_id');
    }

    /**
     * taskDefinition
     *
     * @return BelongsTo
     */
    public function taskDefinition()
    {
        return $this->belongsTo(TaskDefinition::class, 'task_definition_id');
    }

    /**
     * tasks
     *
     * @return HasMany
     */
    public function tasks()
    {
        return $this->hasMany(Task::class, 'task_definition_state_id');
    }
}
