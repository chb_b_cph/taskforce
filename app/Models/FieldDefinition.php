<?php

namespace App\Models;

use Eloquent;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;

/**
 * App\Models\FieldDefinition
 *
 * @property      int $field_type_id field type id
 * @property      string $name name
 * @property      string $description description
 * @property      string $default_value default value
 * @property      string $label label
 * @property      int $length length
 * @property      int $precision precision
 * @property      string $list_of_values_sql list of values sql
 * @property      boolean $active active
 * @property      text $list_of_values list of values
 * @property      FieldType $fieldType belongsTo
 * @property      Collection $clientDefinitionField hasMany
 * @property      Collection $featureDefinitionField hasMany
 * @property      Collection $productDefinitionField hasMany
 * @property      Collection $taskDefinitionField hasMany
 * @property      Collection $userDefinitionField hasMany
 * @property      int $id
 * @property-read Collection|ClientDefinitionField[] $clientDefinitionFields
 * @property-read int|null $client_definition_fields_count
 * @property-read Collection|FeatureDefinitionField[] $featureDefinitionFields
 * @property-read int|null $feature_definition_fields_count
 * @property-read Collection|ProductDefinitionField[] $productDefinitionFields
 * @property-read int|null $product_definition_fields_count
 * @property-read Collection|TaskDefinitionField[] $taskDefinitionFields
 * @property-read int|null $task_definition_fields_count
 * @property-read Collection|UserDefinitionField[] $userDefinitionFields
 * @property-read int|null $user_definition_fields_count
 * @method        static Builder|FieldDefinition newModelQuery()
 * @method        static Builder|FieldDefinition newQuery()
 * @method        static Builder|FieldDefinition query()
 * @method        static Builder|FieldDefinition whereActive($value)
 * @method        static Builder|FieldDefinition whereDefaultValue($value)
 * @method        static Builder|FieldDefinition whereDescription($value)
 * @method        static Builder|FieldDefinition whereFieldTypeId($value)
 * @method        static Builder|FieldDefinition whereId($value)
 * @method        static Builder|FieldDefinition whereLabel($value)
 * @method        static Builder|FieldDefinition whereLength($value)
 * @method        static Builder|FieldDefinition whereListOfValues($value)
 * @method        static Builder|FieldDefinition whereListOfValuesSql($value)
 * @method        static Builder|FieldDefinition whereName($value)
 * @method        static Builder|FieldDefinition wherePrecision($value)
 * @mixin         Eloquent
 */
class FieldDefinition extends Model
{

    /**
     * Database table name
     */
    protected $table = 'field_definitions';

    /**
     * Mass assignable columns
     */
    protected $fillable = ['list_of_values',
        'field_type_id',
        'name',
        'description',
        'default_value',
        'label',
        'length',
        'precision',
        'list_of_values_sql',
        'active',
        'list_of_values'];

    /**
     * Date time columns.
     */
    protected $dates = [];

    /**
     * fieldType
     *
     * @return BelongsTo
     */
    public function fieldType()
    {
        return $this->belongsTo(FieldType::class, 'field_type_id');
    }

    /**
     * clientDefinitionFields
     *
     * @return HasMany
     */
    public function clientDefinitionFields()
    {
        return $this->hasMany(ClientDefinitionField::class, 'field_definition_id');
    }

    /**
     * featureDefinitionFields
     *
     * @return HasMany
     */
    public function featureDefinitionFields()
    {
        return $this->hasMany(FeatureDefinitionField::class, 'field_definition_id');
    }

    /**
     * productDefinitionFields
     *
     * @return HasMany
     */
    public function productDefinitionFields()
    {
        return $this->hasMany(ProductDefinitionField::class, 'field_definition_id');
    }

    /**
     * taskDefinitionFields
     *
     * @return HasMany
     */
    public function taskDefinitionFields()
    {
        return $this->hasMany(TaskDefinitionField::class, 'field_definition_id');
    }

    /**
     * userDefinitionFields
     *
     * @return HasMany
     */
    public function userDefinitionFields()
    {
        return $this->hasMany(UserDefinitionField::class, 'field_definition_id');
    }
}
