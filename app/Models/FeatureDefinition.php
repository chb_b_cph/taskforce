<?php

namespace App\Models;

use Eloquent;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;

/**
 * App\Models\FeatureDefinition
 *
 * @property      string $name name
 * @property      string $description description
 * @property      boolean $active active
 * @property      Collection $featureDefinitionField hasMany
 * @property      Collection $featureDefinitionState hasMany
 * @property      Collection $feature hasMany
 * @property      int $id
 * @property-read Collection|FeatureDefinitionField[] $featureDefinitionFields
 * @property-read int|null $feature_definition_fields_count
 * @property-read Collection|FeatureDefinitionState[] $featureDefinitionStates
 * @property-read int|null $feature_definition_states_count
 * @property-read Collection|Feature[] $features
 * @property-read int|null $features_count
 * @method        static Builder|FeatureDefinition newModelQuery()
 * @method        static Builder|FeatureDefinition newQuery()
 * @method        static Builder|FeatureDefinition query()
 * @method        static Builder|FeatureDefinition whereActive($value)
 * @method        static Builder|FeatureDefinition whereDescription($value)
 * @method        static Builder|FeatureDefinition whereId($value)
 * @method        static Builder|FeatureDefinition whereName($value)
 * @mixin         Eloquent
 */
class FeatureDefinition extends Model
{

    /**
     * Database table name
     */
    protected $table = 'feature_definitions';

    /**
     * Mass assignable columns
     */
    protected $fillable = ['active',
        'name',
        'description',
        'active'];

    /**
     * Date time columns.
     */
    protected $dates = [];

    /**
     * featureDefinitionFields
     *
     * @return HasMany
     */
    public function featureDefinitionFields()
    {
        return $this->hasMany(FeatureDefinitionField::class, 'feature_definition_id');
    }

    /**
     * featureDefinitionStates
     *
     * @return HasMany
     */
    public function featureDefinitionStates()
    {
        return $this->hasMany(FeatureDefinitionState::class, 'feature_definition_id');
    }

    /**
     * features
     *
     * @return HasMany
     */
    public function features()
    {
        return $this->hasMany(Feature::class, 'feature_definition_id');
    }
}
