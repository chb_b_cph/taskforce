<?php

namespace App\Models;

use Eloquent;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;

/**
 * App\Models\TaskDefinition
 *
 * @property      string $name name
 * @property      string $description description
 * @property      boolean $active active
 * @property      string $logo_path logo path
 * @property      string $color_code color code
 * @property      Collection $taskDefinitionField hasMany
 * @property      Collection $taskDefinitionState hasMany
 * @property      Collection $task hasMany
 * @property      int $id
 * @property-read Collection|TaskDefinitionField[] $taskDefinitionFields
 * @property-read int|null $task_definition_fields_count
 * @property-read Collection|TaskDefinitionState[] $taskDefinitionStates
 * @property-read int|null $task_definition_states_count
 * @property-read Collection|Task[] $tasks
 * @property-read int|null $tasks_count
 * @method        static Builder|TaskDefinition newModelQuery()
 * @method        static Builder|TaskDefinition newQuery()
 * @method        static Builder|TaskDefinition query()
 * @method        static Builder|TaskDefinition whereActive($value)
 * @method        static Builder|TaskDefinition whereColorCode($value)
 * @method        static Builder|TaskDefinition whereDescription($value)
 * @method        static Builder|TaskDefinition whereId($value)
 * @method        static Builder|TaskDefinition whereLogoPath($value)
 * @method        static Builder|TaskDefinition whereName($value)
 * @mixin         Eloquent
 */
class TaskDefinition extends Model
{

    /**
     * Database table name
     */
    protected $table = 'task_definitions';

    /**
     * Mass assignable columns
     */
    protected $fillable = ['color_code',
        'name',
        'description',
        'active',
        'logo_path',
        'color_code'];

    /**
     * Date time columns.
     */
    protected $dates = [];

    /**
     * taskDefinitionFields
     *
     * @return HasMany
     */
    public function taskDefinitionFields()
    {
        return $this->hasMany(TaskDefinitionField::class, 'task_definition_id');
    }

    /**
     * taskDefinitionStates
     *
     * @return HasMany
     */
    public function taskDefinitionStates()
    {
        return $this->hasMany(TaskDefinitionState::class, 'task_definition_id');
    }

    /**
     * tasks
     *
     * @return HasMany
     */
    public function tasks()
    {
        return $this->hasMany(Task::class, 'task_definition_id');
    }
}
