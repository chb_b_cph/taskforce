<?php

namespace App\Models;

use Eloquent;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;

/**
 * App\Models\ClientDefinition
 *
 * @property      string $name name
 * @property      string $description description
 * @property      boolean $active active
 * @property      Collection $clientDefinitionField hasMany
 * @property      Collection $clientDefinitionState hasMany
 * @property      Collection $client hasMany
 * @property      int $id
 * @property-read Collection|ClientDefinitionField[] $clientDefinitionFields
 * @property-read int|null $client_definition_fields_count
 * @property-read Collection|ClientDefinitionState[] $clientDefinitionStates
 * @property-read int|null $client_definition_states_count
 * @property-read Collection|Client[] $clients
 * @property-read int|null $clients_count
 * @method        static Builder|ClientDefinition newModelQuery()
 * @method        static Builder|ClientDefinition newQuery()
 * @method        static Builder|ClientDefinition query()
 * @method        static Builder|ClientDefinition whereActive($value)
 * @method        static Builder|ClientDefinition whereDescription($value)
 * @method        static Builder|ClientDefinition whereId($value)
 * @method        static Builder|ClientDefinition whereName($value)
 * @mixin         Eloquent
 */
class ClientDefinition extends Model
{

    /**
     * Database table name
     */
    protected $table = 'client_definitions';

    /**
     * Mass assignable columns
     */
    protected $fillable = ['active',
        'name',
        'description',
        'active'];

    /**
     * Date time columns.
     */
    protected $dates = [];

    /**
     * clientDefinitionFields
     *
     * @return HasMany
     */
    public function clientDefinitionFields()
    {
        return $this->hasMany(ClientDefinitionField::class, 'client_definition_id');
    }

    /**
     * clientDefinitionStates
     *
     * @return HasMany
     */
    public function clientDefinitionStates()
    {
        return $this->hasMany(ClientDefinitionState::class, 'client_definition_id');
    }

    /**
     * clients
     *
     * @return HasMany
     */
    public function clients()
    {
        return $this->hasMany(Client::class, 'client_definition_id');
    }
}
