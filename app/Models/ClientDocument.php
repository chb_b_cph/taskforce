<?php

namespace App\Models;

use Eloquent;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

/**
 * App\Models\ClientDocument
 *
 * @property int $client_id client id
 * @property string $file_name file name
 * @property bigint $file_size file size
 * @property string $mime_type mime type
 * @property blob $file_data file data
 * @property string $description description
 * @property Client $client belongsTo
 * @property int $id
 * @method   static Builder|ClientDocument newModelQuery()
 * @method   static Builder|ClientDocument newQuery()
 * @method   static Builder|ClientDocument query()
 * @method   static Builder|ClientDocument whereClientId($value)
 * @method   static Builder|ClientDocument whereDescription($value)
 * @method   static Builder|ClientDocument whereFileData($value)
 * @method   static Builder|ClientDocument whereFileName($value)
 * @method   static Builder|ClientDocument whereFileSize($value)
 * @method   static Builder|ClientDocument whereId($value)
 * @method   static Builder|ClientDocument whereMimeType($value)
 * @mixin    Eloquent
 */
class ClientDocument extends Model
{

    /**
     * Database table name
     */
    protected $table = 'client_documents';

    /**
     * Mass assignable columns
     */
    protected $fillable = ['description',
        'client_id',
        'file_name',
        'file_size',
        'mime_type',
        'file_data',
        'description'];

    /**
     * Date time columns.
     */
    protected $dates = [];

    /**
     * client
     *
     * @return BelongsTo
     */
    public function client()
    {
        return $this->belongsTo(Client::class, 'client_id');
    }
}
