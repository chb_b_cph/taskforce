<?php

namespace App\Models;

use Eloquent;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;

/**
 * App\Models\Product
 *
 * @property      int $client_id client id
 * @property      int $product_definition_id product definition id
 * @property      int $product_definition_state_id product definition state id
 * @property      string $name name
 * @property      string $description description
 * @property      ProductDefinitionState $productDefinitionState belongsTo
 * @property      ProductDefinition $productDefinition belongsTo
 * @property      Client $client belongsTo
 * @property      Collection $feature hasMany
 * @property      Collection $productComment hasMany
 * @property      Collection $productDocument hasMany
 * @property      Collection $productField hasMany
 * @property      Collection $task hasMany
 * @property      int $id
 * @property-read Collection|Feature[] $features
 * @property-read int|null $features_count
 * @property-read Collection|ProductComment[] $productComments
 * @property-read int|null $product_comments_count
 * @property-read Collection|ProductDocument[] $productDocuments
 * @property-read int|null $product_documents_count
 * @property-read Collection|ProductField[] $productFields
 * @property-read int|null $product_fields_count
 * @property-read Collection|Task[] $tasks
 * @property-read int|null $tasks_count
 * @method        static Builder|Product newModelQuery()
 * @method        static Builder|Product newQuery()
 * @method        static Builder|Product query()
 * @method        static Builder|Product whereClientId($value)
 * @method        static Builder|Product whereDescription($value)
 * @method        static Builder|Product whereId($value)
 * @method        static Builder|Product whereName($value)
 * @method        static Builder|Product whereProductDefinitionId($value)
 * @method        static Builder|Product whereProductDefinitionStateId($value)
 * @mixin         Eloquent
 */
class Product extends Model
{

    /**
     * Database table name
     */
    protected $table = 'products';

    /**
     * Mass assignable columns
     */
    protected $fillable = ['description',
        'client_id',
        'product_definition_id',
        'product_definition_state_id',
        'name',
        'description'];

    /**
     * Date time columns.
     */
    protected $dates = [];

    /**
     * productDefinitionState
     *
     * @return BelongsTo
     */
    public function productDefinitionState()
    {
        return $this->belongsTo(ProductDefinitionState::class, 'product_definition_state_id');
    }

    /**
     * productDefinition
     *
     * @return BelongsTo
     */
    public function productDefinition()
    {
        return $this->belongsTo(ProductDefinition::class, 'product_definition_id');
    }

    /**
     * client
     *
     * @return BelongsTo
     */
    public function client()
    {
        return $this->belongsTo(Client::class, 'client_id');
    }

    /**
     * features
     *
     * @return HasMany
     */
    public function features()
    {
        return $this->hasMany(Feature::class, 'product_id');
    }

    /**
     * productComments
     *
     * @return HasMany
     */
    public function productComments()
    {
        return $this->hasMany(ProductComment::class, 'product_id');
    }

    /**
     * productDocuments
     *
     * @return HasMany
     */
    public function productDocuments()
    {
        return $this->hasMany(ProductDocument::class, 'product_id');
    }

    /**
     * productFields
     *
     * @return HasMany
     */
    public function productFields()
    {
        return $this->hasMany(ProductField::class, 'product_id');
    }

    /**
     * tasks
     *
     * @return HasMany
     */
    public function tasks()
    {
        return $this->hasMany(Task::class, 'product_id');
    }


}
