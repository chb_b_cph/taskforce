<?php

namespace App\Models;

use Eloquent;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

/**
 * App\Models\TeamUser
 *
 * @property int $team_id team id
 * @property int $user_id user id
 * @property User $user belongsTo
 * @property Team $team belongsTo
 * @property int $id
 * @method   static Builder|TeamUser newModelQuery()
 * @method   static Builder|TeamUser newQuery()
 * @method   static Builder|TeamUser query()
 * @method   static Builder|TeamUser whereId($value)
 * @method   static Builder|TeamUser whereTeamId($value)
 * @method   static Builder|TeamUser whereUserId($value)
 * @mixin    Eloquent
 */
class TeamUser extends Model
{

    /**
     * Database table name
     */
    protected $table = 'team_users';

    /**
     * Mass assignable columns
     */
    protected $fillable = ['user_id',
        'team_id',
        'user_id'];

    /**
     * Date time columns.
     */
    protected $dates = [];

    /**
     * user
     *
     * @return BelongsTo
     */
    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }

    /**
     * team
     *
     * @return BelongsTo
     */
    public function team()
    {
        return $this->belongsTo(Team::class, 'team_id');
    }
}
