<?php

namespace App\Models;

use Eloquent;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;

/**
 * App\Models\ClientDefinitionField
 *
 * @property      int $client_definition_id client definition id
 * @property      int $field_definition_id field definition id
 * @property      string $label label
 * @property      int $sort_order sort order
 * @property      boolean $active active
 * @property      string $css css
 * @property      string $js js
 * @property      FieldDefinition $fieldDefinition belongsTo
 * @property      ClientDefinition $clientDefinition belongsTo
 * @property      Collection $clientField hasMany
 * @property      int $id
 * @property-read Collection|ClientField[] $clientFields
 * @property-read int|null $client_fields_count
 * @method        static Builder|ClientDefinitionField newModelQuery()
 * @method        static Builder|ClientDefinitionField newQuery()
 * @method        static Builder|ClientDefinitionField query()
 * @method        static Builder|ClientDefinitionField whereActive($value)
 * @method        static Builder|ClientDefinitionField whereClientDefinitionId($value)
 * @method        static Builder|ClientDefinitionField whereCss($value)
 * @method        static Builder|ClientDefinitionField whereFieldDefinitionId($value)
 * @method        static Builder|ClientDefinitionField whereId($value)
 * @method        static Builder|ClientDefinitionField whereJs($value)
 * @method        static Builder|ClientDefinitionField whereLabel($value)
 * @method        static Builder|ClientDefinitionField whereSortOrder($value)
 * @mixin         Eloquent
 */
class ClientDefinitionField extends Model
{

    /**
     * Database table name
     */
    protected $table = 'client_definition_fields';

    /**
     * Mass assignable columns
     */
    protected $fillable = ['js',
        'client_definition_id',
        'field_definition_id',
        'label',
        'sort_order',
        'active',
        'css',
        'js'];

    /**
     * Date time columns.
     */
    protected $dates = [];

    /**
     * fieldDefinition
     *
     * @return BelongsTo
     */
    public function fieldDefinition()
    {
        return $this->belongsTo(FieldDefinition::class, 'field_definition_id');
    }

    /**
     * clientDefinition
     *
     * @return BelongsTo
     */
    public function clientDefinition()
    {
        return $this->belongsTo(ClientDefinition::class, 'client_definition_id');
    }

    /**
     * clientFields
     *
     * @return HasMany
     */
    public function clientFields()
    {
        return $this->hasMany(ClientField::class, 'client_definition_field_id');
    }
}
