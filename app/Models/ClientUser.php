<?php

namespace App\Models;

use Eloquent;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

/**
 * App\Models\ClientUser
 *
 * @property int $client_id client id
 * @property int $user_id user id
 * @property User $user belongsTo
 * @property Client $client belongsTo
 * @property int $id
 * @method   static Builder|ClientUser newModelQuery()
 * @method   static Builder|ClientUser newQuery()
 * @method   static Builder|ClientUser query()
 * @method   static Builder|ClientUser whereClientId($value)
 * @method   static Builder|ClientUser whereId($value)
 * @method   static Builder|ClientUser whereUserId($value)
 * @mixin    Eloquent
 */
class ClientUser extends Model
{

    /**
     * Database table name
     */
    protected $table = 'client_users';

    /**
     * Mass assignable columns
     */
    protected $fillable = ['user_id',
        'client_id',
        'user_id'];

    /**
     * Date time columns.
     */
    protected $dates = [];

    /**
     * user
     *
     * @return BelongsTo
     */
    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }

    /**
     * client
     *
     * @return BelongsTo
     */
    public function client()
    {
        return $this->belongsTo(Client::class, 'client_id');
    }
}
