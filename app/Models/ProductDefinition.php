<?php

namespace App\Models;

use Eloquent;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;

/**
 * App\Models\ProductDefinition
 *
 * @property      string $name name
 * @property      string $description description
 * @property      boolean $active active
 * @property      Collection $productDefinitionField hasMany
 * @property      Collection $productDefinitionState hasMany
 * @property      Collection $product hasMany
 * @property      int $id
 * @property-read Collection|ProductDefinitionField[] $productDefinitionFields
 * @property-read int|null $product_definition_fields_count
 * @property-read Collection|ProductDefinitionState[] $productDefinitionStates
 * @property-read int|null $product_definition_states_count
 * @property-read Collection|Product[] $products
 * @property-read int|null $products_count
 * @method        static Builder|ProductDefinition newModelQuery()
 * @method        static Builder|ProductDefinition newQuery()
 * @method        static Builder|ProductDefinition query()
 * @method        static Builder|ProductDefinition whereActive($value)
 * @method        static Builder|ProductDefinition whereDescription($value)
 * @method        static Builder|ProductDefinition whereId($value)
 * @method        static Builder|ProductDefinition whereName($value)
 * @mixin         Eloquent
 */
class ProductDefinition extends Model
{

    /**
     * Database table name
     */
    protected $table = 'product_definitions';

    /**
     * Mass assignable columns
     */
    protected $fillable = ['active',
        'name',
        'description',
        'active'];

    /**
     * Date time columns.
     */
    protected $dates = [];

    /**
     * productDefinitionFields
     *
     * @return HasMany
     */
    public function productDefinitionFields()
    {
        return $this->hasMany(ProductDefinitionField::class, 'product_definition_id');
    }

    /**
     * productDefinitionStates
     *
     * @return HasMany
     */
    public function productDefinitionStates()
    {
        return $this->hasMany(ProductDefinitionState::class, 'product_definition_id');
    }

    /**
     * products
     *
     * @return HasMany
     */
    public function products()
    {
        return $this->hasMany(Product::class, 'product_definition_id');
    }
}
