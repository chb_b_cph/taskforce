<?php

namespace App\Models;

use Eloquent;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;

/**
 * App\Models\Task
 *
 * @property      int $product_id product id
 * @property      int $feature_id feature id
 * @property      int $task_definition_id task definition id
 * @property      int $task_definition_state_id task definition state id
 * @property      string $name name
 * @property      string $description description
 * @property      int $client_visible client visible
 * @property      TaskDefinitionState $taskDefinitionState belongsTo
 * @property      TaskDefinition $taskDefinition belongsTo
 * @property      Feature $feature belongsTo
 * @property      Product $product belongsTo
 * @property      Collection $taskComment hasMany
 * @property      Collection $taskDocument hasMany
 * @property      Collection $taskField hasMany
 * @property      Collection $teamBoardColumnTask hasMany
 * @property      int $id
 * @property-read Collection|TaskComment[] $taskComments
 * @property-read int|null $task_comments_count
 * @property-read Collection|TaskDocument[] $taskDocuments
 * @property-read int|null $task_documents_count
 * @property-read Collection|TaskField[] $taskFields
 * @property-read int|null $task_fields_count
 * @property-read Collection|TeamBoardColumnTask[] $teamBoardColumnTasks
 * @property-read int|null $team_board_column_tasks_count
 * @method        static Builder|Task newModelQuery()
 * @method        static Builder|Task newQuery()
 * @method        static Builder|Task query()
 * @method        static Builder|Task whereClientVisible($value)
 * @method        static Builder|Task whereDescription($value)
 * @method        static Builder|Task whereFeatureId($value)
 * @method        static Builder|Task whereId($value)
 * @method        static Builder|Task whereName($value)
 * @method        static Builder|Task whereProductId($value)
 * @method        static Builder|Task whereTaskDefinitionId($value)
 * @method        static Builder|Task whereTaskDefinitionStateId($value)
 * @mixin         Eloquent
 */
class Task extends Model
{

    /**
     * Database table name
     */
    protected $table = 'tasks';

    /**
     * Mass assignable columns
     */
    protected $fillable = ['client_visible',
        'product_id',
        'feature_id',
        'task_definition_id',
        'task_definition_state_id',
        'name',
        'description',
        'client_visible'];

    /**
     * Date time columns.
     */
    protected $dates = [];

    /**
     * taskDefinitionState
     *
     * @return BelongsTo
     */
    public function taskDefinitionState()
    {
        return $this->belongsTo(TaskDefinitionState::class, 'task_definition_state_id');
    }

    /**
     * taskDefinition
     *
     * @return BelongsTo
     */
    public function taskDefinition()
    {
        return $this->belongsTo(TaskDefinition::class, 'task_definition_id');
    }

    /**
     * feature
     *
     * @return BelongsTo
     */
    public function feature()
    {
        return $this->belongsTo(Feature::class, 'feature_id');
    }

    /**
     * product
     *
     * @return BelongsTo
     */
    public function product()
    {
        return $this->belongsTo(Product::class, 'product_id');
    }

    /**
     * taskComments
     *
     * @return HasMany
     */
    public function taskComments()
    {
        return $this->hasMany(TaskComment::class, 'task_id');
    }

    /**
     * taskDocuments
     *
     * @return HasMany
     */
    public function taskDocuments()
    {
        return $this->hasMany(TaskDocument::class, 'task_id');
    }

    /**
     * taskFields
     *
     * @return HasMany
     */
    public function taskFields()
    {
        return $this->hasMany(TaskField::class, 'task_id');
    }

    /**
     * teamBoardColumnTasks
     *
     * @return HasMany
     */
    public function teamBoardColumnTasks()
    {
        return $this->hasMany(TeamBoardColumnTask::class, 'task_id');
    }
}
