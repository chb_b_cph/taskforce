<?php

namespace App\Models;

use Eloquent;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

/**
 * App\Models\TeamBoardColumnTask
 *
 * @property int $team_board_column_id team board column id
 * @property int $task_id task id
 * @property boolean $is_done is done
 * @property int $sort_order sort order
 * @property Task $task belongsTo
 * @property TeamBoardColumn $teamBoardColumn belongsTo
 * @property int $id
 * @method   static Builder|TeamBoardColumnTask newModelQuery()
 * @method   static Builder|TeamBoardColumnTask newQuery()
 * @method   static Builder|TeamBoardColumnTask query()
 * @method   static Builder|TeamBoardColumnTask whereId($value)
 * @method   static Builder|TeamBoardColumnTask whereIsDone($value)
 * @method   static Builder|TeamBoardColumnTask whereSortOrder($value)
 * @method   static Builder|TeamBoardColumnTask whereTaskId($value)
 * @method   static Builder|TeamBoardColumnTask whereTeamBoardColumnId($value)
 * @mixin    Eloquent
 */
class TeamBoardColumnTask extends Model
{

    /**
     * Database table name
     */
    protected $table = 'team_board_column_tasks';

    /**
     * Mass assignable columns
     */
    protected $fillable = ['sort_order',
        'team_board_column_id',
        'task_id',
        'is_done',
        'sort_order'];

    /**
     * Date time columns.
     */
    protected $dates = [];

    /**
     * task
     *
     * @return BelongsTo
     */
    public function task()
    {
        return $this->belongsTo(Task::class, 'task_id');
    }

    /**
     * teamBoardColumn
     *
     * @return BelongsTo
     */
    public function teamBoardColumn()
    {
        return $this->belongsTo(TeamBoardColumn::class, 'team_board_column_id');
    }
}
