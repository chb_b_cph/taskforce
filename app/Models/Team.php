<?php

namespace App\Models;

use Eloquent;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;

/**
 * App\Models\Team
 *
 * @property      string $name name
 * @property      Collection $teamBoard hasMany
 * @property      Collection $teamUser hasMany
 * @property      int $id
 * @property-read Collection|TeamBoard[] $teamBoards
 * @property-read int|null $team_boards_count
 * @property-read Collection|TeamUser[] $teamUsers
 * @property-read int|null $team_users_count
 * @method        static Builder|Team newModelQuery()
 * @method        static Builder|Team newQuery()
 * @method        static Builder|Team query()
 * @method        static Builder|Team whereId($value)
 * @method        static Builder|Team whereName($value)
 * @mixin         Eloquent
 */
class Team extends Model
{

    /**
     * Database table name
     */
    protected $table = 'teams';

    /**
     * Mass assignable columns
     */
    protected $fillable = ['name',
        'name'];

    /**
     * Date time columns.
     */
    protected $dates = [];

    /**
     * teamBoards
     *
     * @return HasMany
     */
    public function teamBoards()
    {
        return $this->hasMany(TeamBoard::class, 'team_id');
    }

    /**
     * teamUsers
     *
     * @return HasMany
     */
    public function teamUsers()
    {
        return $this->hasMany(TeamUser::class, 'team_id');
    }
}
