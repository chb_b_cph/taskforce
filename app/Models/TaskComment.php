<?php

namespace App\Models;

use Eloquent;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

/**
 * App\Models\TaskComment
 *
 * @property int $task_id task id
 * @property string $value value
 * @property boolean $client_visible client visible
 * @property Task $task belongsTo
 * @property int $id
 * @method   static Builder|TaskComment newModelQuery()
 * @method   static Builder|TaskComment newQuery()
 * @method   static Builder|TaskComment query()
 * @method   static Builder|TaskComment whereClientVisible($value)
 * @method   static Builder|TaskComment whereId($value)
 * @method   static Builder|TaskComment whereTaskId($value)
 * @method   static Builder|TaskComment whereValue($value)
 * @mixin    Eloquent
 */
class TaskComment extends Model
{

    /**
     * Database table name
     */
    protected $table = 'task_comments';

    /**
     * Mass assignable columns
     */
    protected $fillable = ['client_visible',
        'task_id',
        'value',
        'client_visible'];

    /**
     * Date time columns.
     */
    protected $dates = [];

    /**
     * task
     *
     * @return BelongsTo
     */
    public function task()
    {
        return $this->belongsTo(Task::class, 'task_id');
    }
}
