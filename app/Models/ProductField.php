<?php

namespace App\Models;

use Eloquent;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

/**
 * App\Models\ProductField
 *
 * @property int $product_id product id
 * @property int $product_definition_field_id product definition field id
 * @property string $value value
 * @property ProductDefinitionField $productDefinitionField belongsTo
 * @property Product $product belongsTo
 * @property int $id
 * @method   static Builder|ProductField newModelQuery()
 * @method   static Builder|ProductField newQuery()
 * @method   static Builder|ProductField query()
 * @method   static Builder|ProductField whereId($value)
 * @method   static Builder|ProductField whereProductDefinitionFieldId($value)
 * @method   static Builder|ProductField whereProductId($value)
 * @method   static Builder|ProductField whereValue($value)
 * @mixin    Eloquent
 */
class ProductField extends Model
{

    /**
     * Database table name
     */
    protected $table = 'product_fields';

    /**
     * Mass assignable columns
     */
    protected $fillable = ['value',
        'product_id',
        'product_definition_field_id',
        'value'];

    /**
     * Date time columns.
     */
    protected $dates = [];

    /**
     * productDefinitionField
     *
     * @return BelongsTo
     */
    public function productDefinitionField()
    {
        return $this->belongsTo(ProductDefinitionField::class, 'product_definition_field_id');
    }

    /**
     * product
     *
     * @return BelongsTo
     */
    public function product()
    {
        return $this->belongsTo(Product::class, 'product_id');
    }
}
