<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateFeatureFieldsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('feature_fields', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->integer('feature_id')->index('feature_fields_feature_id_idx');
			$table->integer('feature_definition_field_id')->index('feature_fields_feature_definition_field_id_idx');
			$table->string('value', 4000)->nullable();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('feature_fields');
	}

}
