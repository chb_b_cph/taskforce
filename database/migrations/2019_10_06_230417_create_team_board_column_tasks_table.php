<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateTeamBoardColumnTasksTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('team_board_column_tasks', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->integer('team_board_column_id')->index('team_board_column_tasks_team_board_column_id_idx');
			$table->integer('task_id')->index('team_board_column_tasks_task_id_idx');
			$table->boolean('is_done');
			$table->smallInteger('sort_order');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('team_board_column_tasks');
	}

}
