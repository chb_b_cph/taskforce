<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateClientDefinitionStatesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('client_definition_states', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->smallInteger('client_definition_id')->index('client_definition_states_client_definition_id_idx');
			$table->smallInteger('state_type_id')->index('client_definition_states_state_type_id_idx');
			$table->smallInteger('sort_order');
			$table->string('name')->nullable();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('client_definition_states');
	}

}
