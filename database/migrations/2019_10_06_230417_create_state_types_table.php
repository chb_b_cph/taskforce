<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateStateTypesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('state_types', function(Blueprint $table)
		{
			$table->smallInteger('id', true);
			$table->string('name');
			$table->string('description', 4000)->nullable();
			$table->string('data_type', 50);
			$table->smallInteger('default_length');
			$table->smallInteger('default_precision')->nullable();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('state_types');
	}

}
