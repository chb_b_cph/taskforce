<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateTasksTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('tasks', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->integer('product_id')->index('tasks_product_id_idx');
			$table->integer('feature_id')->nullable()->index('tasks_feature_id_idx');
			$table->integer('task_definition_id')->index('tasks_task_definition_id_idx');
			$table->integer('task_definition_state_id')->index('tasks_task_definition_state_id_idx');
			$table->string('name')->nullable();
			$table->string('description', 4000)->nullable();
			$table->boolean('client_visible')->default(0);
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('tasks');
	}

}
