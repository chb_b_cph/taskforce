<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToClientUsersTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('client_users', function(Blueprint $table)
		{
			$table->foreign('client_id', 'lnk_clients_client_users')->references('id')->on('clients')->onUpdate('CASCADE')->onDelete('CASCADE');
			$table->foreign('user_id', 'lnk_users_client_users')->references('id')->on('users')->onUpdate('CASCADE')->onDelete('CASCADE');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('client_users', function(Blueprint $table)
		{
			$table->dropForeign('lnk_clients_client_users');
			$table->dropForeign('lnk_users_client_users');
		});
	}

}
