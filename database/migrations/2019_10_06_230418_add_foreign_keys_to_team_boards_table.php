<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToTeamBoardsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('team_boards', function(Blueprint $table)
		{
			$table->foreign('team_id', 'lnk_teams_team_boards')->references('id')->on('teams')->onUpdate('CASCADE')->onDelete('CASCADE');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('team_boards', function(Blueprint $table)
		{
			$table->dropForeign('lnk_teams_team_boards');
		});
	}

}
