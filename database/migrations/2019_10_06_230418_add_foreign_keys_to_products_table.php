<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToProductsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('products', function(Blueprint $table)
		{
			$table->foreign('client_id', 'lnk_clients_products')->references('id')->on('clients')->onUpdate('CASCADE')->onDelete('CASCADE');
			$table->foreign('product_definition_id', 'lnk_product_definitions_products')->references('id')->on('product_definitions')->onUpdate('CASCADE')->onDelete('CASCADE');
			$table->foreign('product_definition_state_id', 'lnk_product_definition_states_products')->references('id')->on('product_definition_states')->onUpdate('CASCADE')->onDelete('CASCADE');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('products', function(Blueprint $table)
		{
			$table->dropForeign('lnk_clients_products');
			$table->dropForeign('lnk_product_definitions_products');
			$table->dropForeign('lnk_product_definition_states_products');
		});
	}

}
