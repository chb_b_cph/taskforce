<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToFeatureDefinitionStatesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('feature_definition_states', function(Blueprint $table)
		{
			$table->foreign('feature_definition_id', 'lnk_feature_definitions_feature_definition_states')->references('id')->on('feature_definitions')->onUpdate('CASCADE')->onDelete('CASCADE');
			$table->foreign('state_type_id', 'lnk_state_types_feature_definition_states')->references('id')->on('state_types')->onUpdate('CASCADE')->onDelete('CASCADE');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('feature_definition_states', function(Blueprint $table)
		{
			$table->dropForeign('lnk_feature_definitions_feature_definition_states');
			$table->dropForeign('lnk_state_types_feature_definition_states');
		});
	}

}
