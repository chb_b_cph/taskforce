<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToFeaturesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('features', function(Blueprint $table)
		{
			$table->foreign('product_id', 'lnk_products_features')->references('id')->on('products')->onUpdate('CASCADE')->onDelete('CASCADE');
			$table->foreign('feature_definition_id', 'lnk_feature_definitions_features')->references('id')->on('feature_definitions')->onUpdate('CASCADE')->onDelete('CASCADE');
			$table->foreign('feature_definition_state_id', 'lnk_feature_definition_states_features')->references('id')->on('feature_definition_states')->onUpdate('CASCADE')->onDelete('CASCADE');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('features', function(Blueprint $table)
		{
			$table->dropForeign('lnk_products_features');
			$table->dropForeign('lnk_feature_definitions_features');
			$table->dropForeign('lnk_feature_definition_states_features');
		});
	}

}
