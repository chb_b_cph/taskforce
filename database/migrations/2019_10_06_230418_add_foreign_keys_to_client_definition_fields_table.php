<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToClientDefinitionFieldsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('client_definition_fields', function(Blueprint $table)
		{
			$table->foreign('field_definition_id', 'lnk_field_definitions_client_definition_fields')->references('id')->on('field_definitions')->onUpdate('CASCADE')->onDelete('CASCADE');
			$table->foreign('client_definition_id', 'lnk_client_definitions_client_definition_fields')->references('id')->on('client_definitions')->onUpdate('CASCADE')->onDelete('CASCADE');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('client_definition_fields', function(Blueprint $table)
		{
			$table->dropForeign('lnk_field_definitions_client_definition_fields');
			$table->dropForeign('lnk_client_definitions_client_definition_fields');
		});
	}

}
