<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateFeaturesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('features', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->integer('product_id')->index('features_product_id_idx');
			$table->integer('feature_definition_id')->index('features_feature_definition_id_idx');
			$table->integer('feature_definition_state_id')->index('features_feature_definition_state_id_idx');
			$table->string('name')->nullable();
			$table->string('description', 4000)->nullable();
			$table->boolean('client_visible')->default(0);
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('features');
	}

}
