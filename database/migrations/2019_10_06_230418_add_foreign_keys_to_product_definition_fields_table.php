<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToProductDefinitionFieldsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('product_definition_fields', function(Blueprint $table)
		{
			$table->foreign('product_definition_id', 'lnk_product_definitions_product_definition_fields')->references('id')->on('product_definitions')->onUpdate('CASCADE')->onDelete('CASCADE');
			$table->foreign('field_definition_id', 'lnk_field_definitions_product_definition_fields')->references('id')->on('field_definitions')->onUpdate('CASCADE')->onDelete('CASCADE');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('product_definition_fields', function(Blueprint $table)
		{
			$table->dropForeign('lnk_product_definitions_product_definition_fields');
			$table->dropForeign('lnk_field_definitions_product_definition_fields');
		});
	}

}
