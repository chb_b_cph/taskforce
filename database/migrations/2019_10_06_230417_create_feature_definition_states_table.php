<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateFeatureDefinitionStatesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('feature_definition_states', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->integer('feature_definition_id')->index('feature_definition_states_feature_definition_id_idx');
			$table->smallInteger('state_type_id')->index('feature_definition_states_state_type_id_idx');
			$table->smallInteger('sort_order');
			$table->string('name')->nullable();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('feature_definition_states');
	}

}
