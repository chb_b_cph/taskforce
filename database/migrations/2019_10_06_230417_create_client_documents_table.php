<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateClientDocumentsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('client_documents', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->integer('client_id')->index('client_documents_client_id_idx');
			$table->string('file_name')->nullable();
			$table->bigInteger('file_size')->nullable();
			$table->string('mime_type')->nullable();
			$table->binary('file_data')->nullable();
			$table->string('description', 4000)->nullable();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('client_documents');
	}

}
