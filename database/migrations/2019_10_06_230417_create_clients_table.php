<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateClientsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('clients', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->smallInteger('client_definition_id')->index('clients_client_definition_id_idx');
			$table->integer('client_definition_state_id')->index('clients_client_definition_state_id_idx');
			$table->string('name')->unique('clients_name_key');
			$table->string('description', 4000)->nullable();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('clients');
	}

}
