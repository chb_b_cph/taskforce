<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToTeamUsersTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('team_users', function(Blueprint $table)
		{
			$table->foreign('team_id', 'lnk_teams_team_users')->references('id')->on('teams')->onUpdate('CASCADE')->onDelete('CASCADE');
			$table->foreign('user_id', 'lnk_users_team_users')->references('id')->on('users')->onUpdate('CASCADE')->onDelete('CASCADE');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('team_users', function(Blueprint $table)
		{
			$table->dropForeign('lnk_teams_team_users');
			$table->dropForeign('lnk_users_team_users');
		});
	}

}
