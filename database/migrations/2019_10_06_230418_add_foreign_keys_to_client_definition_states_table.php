<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToClientDefinitionStatesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('client_definition_states', function(Blueprint $table)
		{
			$table->foreign('state_type_id', 'lnk_state_types_client_definition_states')->references('id')->on('state_types')->onUpdate('CASCADE')->onDelete('CASCADE');
			$table->foreign('client_definition_id', 'lnk_client_definitions_client_definition_states')->references('id')->on('client_definitions')->onUpdate('CASCADE')->onDelete('CASCADE');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('client_definition_states', function(Blueprint $table)
		{
			$table->dropForeign('lnk_state_types_client_definition_states');
			$table->dropForeign('lnk_client_definitions_client_definition_states');
		});
	}

}
