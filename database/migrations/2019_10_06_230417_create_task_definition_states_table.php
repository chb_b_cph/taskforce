<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateTaskDefinitionStatesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('task_definition_states', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->integer('task_definition_id')->index('task_definition_states_task_definition_id_idx');
			$table->smallInteger('state_type_id')->index('task_definition_states_state_type_id_idx');
			$table->smallInteger('sort_order');
			$table->string('name')->nullable();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('task_definition_states');
	}

}
