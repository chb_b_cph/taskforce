<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateTeamBoardsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('team_boards', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->string('name');
			$table->string('description', 4000)->nullable();
			$table->string('logo_path', 2044);
			$table->string('color_code', 20)->nullable();
			$table->integer('team_id')->index('index_team_id');
			$table->string('type', 10);
			$table->string('css', 4000)->nullable();
			$table->string('js', 4000)->nullable();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('team_boards');
	}

}
