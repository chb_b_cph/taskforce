<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateTeamBoardColumnsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('team_board_columns', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->string('name');
			$table->string('description', 4000)->nullable();
			$table->boolean('split_column')->default(0);
			$table->smallInteger('sort_order');
			$table->integer('team_board_id')->index('team_board_columns_team_board_id_idx');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('team_board_columns');
	}

}
