<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateProductsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('products', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->integer('client_id')->index('products_client_id_idx');
			$table->integer('product_definition_id')->index('products_product_definition_id_idx');
			$table->integer('product_definition_state_id')->index('products_product_definition_state_id_idx');
			$table->string('name')->nullable();
			$table->string('description', 4000)->nullable();
			$table->unique(['client_id','name'], 'products_client_id_name_key');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('products');
	}

}
