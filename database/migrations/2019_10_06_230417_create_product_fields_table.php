<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateProductFieldsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('product_fields', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->integer('product_id')->index('product_fields_product_id_idx');
			$table->integer('product_definition_field_id')->index('product_fields_product_definition_field_id_idx');
			$table->string('value', 4000)->nullable();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('product_fields');
	}

}
