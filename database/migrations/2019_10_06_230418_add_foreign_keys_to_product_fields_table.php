<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToProductFieldsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('product_fields', function(Blueprint $table)
		{
			$table->foreign('product_id', 'lnk_products_product_fields')->references('id')->on('products')->onUpdate('CASCADE')->onDelete('CASCADE');
			$table->foreign('product_definition_field_id', 'lnk_product_definition_fields_product_fields')->references('id')->on('product_definition_fields')->onUpdate('CASCADE')->onDelete('CASCADE');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('product_fields', function(Blueprint $table)
		{
			$table->dropForeign('lnk_products_product_fields');
			$table->dropForeign('lnk_product_definition_fields_product_fields');
		});
	}

}
