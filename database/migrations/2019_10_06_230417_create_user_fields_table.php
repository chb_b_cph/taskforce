<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateUserFieldsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('user_fields', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->integer('user_id')->index('user_fields_user_id_idx');
			$table->integer('user_definition_field_id')->index('user_fields_user_definition_field_id_idx');
			$table->string('value', 4000)->nullable();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('user_fields');
	}

}
