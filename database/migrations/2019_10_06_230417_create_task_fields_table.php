<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateTaskFieldsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('task_fields', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->integer('task_id')->index('task_fields_task_id_idx');
			$table->integer('task_definition_field_id')->index('task_fields_task_definition_field_id_idx');
			$table->string('value', 4000)->nullable();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('task_fields');
	}

}
