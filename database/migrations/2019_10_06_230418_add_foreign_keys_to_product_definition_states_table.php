<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToProductDefinitionStatesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('product_definition_states', function(Blueprint $table)
		{
			$table->foreign('product_definition_id', 'lnk_product_definitions_product_definition_states')->references('id')->on('product_definitions')->onUpdate('CASCADE')->onDelete('CASCADE');
			$table->foreign('state_type_id', 'lnk_state_types_product_definition_states')->references('id')->on('state_types')->onUpdate('CASCADE')->onDelete('CASCADE');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('product_definition_states', function(Blueprint $table)
		{
			$table->dropForeign('lnk_product_definitions_product_definition_states');
			$table->dropForeign('lnk_state_types_product_definition_states');
		});
	}

}
