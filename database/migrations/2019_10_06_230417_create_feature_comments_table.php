<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateFeatureCommentsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('feature_comments', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->integer('feature_id')->index('feature_comments_feature_id_idx');
			$table->string('value', 4000)->nullable();
			$table->boolean('client_visible')->default(0);
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('feature_comments');
	}

}
