<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateClientUsersTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('client_users', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->integer('client_id')->index('client_users_client_id_idx');
			$table->integer('user_id')->index('client_users_user_id_idx');
			$table->unique(['client_id','user_id'], 'client_users_client_id_user_id_key');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('client_users');
	}

}
