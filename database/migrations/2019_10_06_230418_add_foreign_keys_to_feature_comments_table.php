<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToFeatureCommentsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('feature_comments', function(Blueprint $table)
		{
			$table->foreign('feature_id', 'lnk_features_feature_comments')->references('id')->on('features')->onUpdate('CASCADE')->onDelete('CASCADE');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('feature_comments', function(Blueprint $table)
		{
			$table->dropForeign('lnk_features_feature_comments');
		});
	}

}
