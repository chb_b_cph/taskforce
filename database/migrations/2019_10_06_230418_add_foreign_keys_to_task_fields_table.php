<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToTaskFieldsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('task_fields', function(Blueprint $table)
		{
			$table->foreign('task_id', 'lnk_tasks_task_fields')->references('id')->on('tasks')->onUpdate('CASCADE')->onDelete('CASCADE');
			$table->foreign('task_definition_field_id', 'lnk_task_definition_fields_task_fields')->references('id')->on('task_definition_fields')->onUpdate('CASCADE')->onDelete('CASCADE');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('task_fields', function(Blueprint $table)
		{
			$table->dropForeign('lnk_tasks_task_fields');
			$table->dropForeign('lnk_task_definition_fields_task_fields');
		});
	}

}
