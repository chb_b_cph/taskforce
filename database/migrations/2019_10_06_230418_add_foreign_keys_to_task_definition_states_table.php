<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToTaskDefinitionStatesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('task_definition_states', function(Blueprint $table)
		{
			$table->foreign('task_definition_id', 'lnk_task_definitions_task_definition_states')->references('id')->on('task_definitions')->onUpdate('CASCADE')->onDelete('CASCADE');
			$table->foreign('state_type_id', 'lnk_state_types_task_definition_states')->references('id')->on('state_types')->onUpdate('CASCADE')->onDelete('CASCADE');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('task_definition_states', function(Blueprint $table)
		{
			$table->dropForeign('lnk_task_definitions_task_definition_states');
			$table->dropForeign('lnk_state_types_task_definition_states');
		});
	}

}
