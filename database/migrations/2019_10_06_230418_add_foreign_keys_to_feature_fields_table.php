<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToFeatureFieldsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('feature_fields', function(Blueprint $table)
		{
			$table->foreign('feature_id', 'lnk_features_feature_fields')->references('id')->on('features')->onUpdate('CASCADE')->onDelete('CASCADE');
			$table->foreign('feature_definition_field_id', 'lnk_feature_definition_fields_feature_fields')->references('id')->on('feature_definition_fields')->onUpdate('CASCADE')->onDelete('CASCADE');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('feature_fields', function(Blueprint $table)
		{
			$table->dropForeign('lnk_features_feature_fields');
			$table->dropForeign('lnk_feature_definition_fields_feature_fields');
		});
	}

}
