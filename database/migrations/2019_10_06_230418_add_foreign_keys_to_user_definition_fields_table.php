<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToUserDefinitionFieldsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('user_definition_fields', function(Blueprint $table)
		{
			$table->foreign('user_definition_id', 'lnk_user_definitions_user_definition_fields')->references('id')->on('user_definitions')->onUpdate('CASCADE')->onDelete('CASCADE');
			$table->foreign('field_definition_id', 'lnk_field_definitions_user_definition_fields')->references('id')->on('field_definitions')->onUpdate('CASCADE')->onDelete('CASCADE');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('user_definition_fields', function(Blueprint $table)
		{
			$table->dropForeign('lnk_user_definitions_user_definition_fields');
			$table->dropForeign('lnk_field_definitions_user_definition_fields');
		});
	}

}
