<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateClientDefinitionFieldsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('client_definition_fields', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->smallInteger('client_definition_id')->index('client_definition_fields_client_definition_id_idx');
			$table->smallInteger('field_definition_id')->index('client_definition_fields_field_definition_id_idx');
			$table->string('label')->nullable();
			$table->smallInteger('sort_order');
			$table->boolean('active')->nullable()->default(1);
			$table->string('css', 4000)->nullable();
			$table->string('js', 4000)->nullable();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('client_definition_fields');
	}

}
