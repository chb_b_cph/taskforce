<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToClientFieldsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('client_fields', function(Blueprint $table)
		{
			$table->foreign('client_id', 'lnk_clients_client_fields')->references('id')->on('clients')->onUpdate('CASCADE')->onDelete('CASCADE');
			$table->foreign('client_definition_field_id', 'lnk_client_definition_fields_client_fields')->references('id')->on('client_definition_fields')->onUpdate('CASCADE')->onDelete('CASCADE');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('client_fields', function(Blueprint $table)
		{
			$table->dropForeign('lnk_clients_client_fields');
			$table->dropForeign('lnk_client_definition_fields_client_fields');
		});
	}

}
