<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToTaskDefinitionFieldsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('task_definition_fields', function(Blueprint $table)
		{
			$table->foreign('task_definition_id', 'lnk_task_definitions_task_definition_fields')->references('id')->on('task_definitions')->onUpdate('CASCADE')->onDelete('CASCADE');
			$table->foreign('field_definition_id', 'lnk_field_definitions_task_definition_fields')->references('id')->on('field_definitions')->onUpdate('CASCADE')->onDelete('CASCADE');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('task_definition_fields', function(Blueprint $table)
		{
			$table->dropForeign('lnk_task_definitions_task_definition_fields');
			$table->dropForeign('lnk_field_definitions_task_definition_fields');
		});
	}

}
