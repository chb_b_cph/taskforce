<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateClientFieldsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('client_fields', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->integer('client_id')->index('client_fields_client_id_idx');
			$table->integer('client_definition_field_id')->index('client_fields_client_definition_field_id_idx');
			$table->string('value', 4000)->nullable();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('client_fields');
	}

}
