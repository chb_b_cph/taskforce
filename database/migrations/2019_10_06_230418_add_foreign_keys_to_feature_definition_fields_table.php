<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToFeatureDefinitionFieldsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('feature_definition_fields', function(Blueprint $table)
		{
			$table->foreign('feature_definition_id', 'lnk_feature_definitions_feature_definition_fields')->references('id')->on('feature_definitions')->onUpdate('CASCADE')->onDelete('CASCADE');
			$table->foreign('field_definition_id', 'lnk_field_definitions_feature_definition_fields')->references('id')->on('field_definitions')->onUpdate('CASCADE')->onDelete('CASCADE');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('feature_definition_fields', function(Blueprint $table)
		{
			$table->dropForeign('lnk_feature_definitions_feature_definition_fields');
			$table->dropForeign('lnk_field_definitions_feature_definition_fields');
		});
	}

}
