<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateProductDocumentsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('product_documents', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->integer('product_id')->index('product_documents_product_id_idx');
			$table->string('file_name')->nullable();
			$table->bigInteger('file_size')->nullable();
			$table->string('mime_type')->nullable();
			$table->binary('file_data')->nullable();
			$table->string('description', 4000)->nullable();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('product_documents');
	}

}
