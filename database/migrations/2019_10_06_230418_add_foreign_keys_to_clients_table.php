<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToClientsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('clients', function(Blueprint $table)
		{
			$table->foreign('client_definition_id', 'lnk_client_definitions_clients')->references('id')->on('client_definitions')->onUpdate('CASCADE')->onDelete('CASCADE');
			$table->foreign('client_definition_state_id', 'lnk_client_definition_states_clients')->references('id')->on('client_definition_states')->onUpdate('CASCADE')->onDelete('CASCADE');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('clients', function(Blueprint $table)
		{
			$table->dropForeign('lnk_client_definitions_clients');
			$table->dropForeign('lnk_client_definition_states_clients');
		});
	}

}
