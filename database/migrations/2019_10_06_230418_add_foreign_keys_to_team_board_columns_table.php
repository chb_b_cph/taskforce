<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToTeamBoardColumnsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('team_board_columns', function(Blueprint $table)
		{
			$table->foreign('team_board_id', 'lnk_team_boards_team_board_columns')->references('id')->on('team_boards')->onUpdate('CASCADE')->onDelete('CASCADE');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('team_board_columns', function(Blueprint $table)
		{
			$table->dropForeign('lnk_team_boards_team_board_columns');
		});
	}

}
