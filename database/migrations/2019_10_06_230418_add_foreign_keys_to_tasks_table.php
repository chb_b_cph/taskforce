<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToTasksTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('tasks', function(Blueprint $table)
		{
			$table->foreign('feature_id', 'lnk_features_tasks')->references('id')->on('features')->onUpdate('CASCADE')->onDelete('CASCADE');
			$table->foreign('product_id', 'lnk_products_tasks')->references('id')->on('products')->onUpdate('CASCADE')->onDelete('CASCADE');
			$table->foreign('task_definition_id', 'lnk_task_definitions_tasks')->references('id')->on('task_definitions')->onUpdate('CASCADE')->onDelete('CASCADE');
			$table->foreign('task_definition_state_id', 'lnk_task_definition_states_tasks')->references('id')->on('task_definition_states')->onUpdate('CASCADE')->onDelete('CASCADE');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('tasks', function(Blueprint $table)
		{
			$table->dropForeign('lnk_features_tasks');
			$table->dropForeign('lnk_products_tasks');
			$table->dropForeign('lnk_task_definitions_tasks');
			$table->dropForeign('lnk_task_definition_states_tasks');
		});
	}

}
