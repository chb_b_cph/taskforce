<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToUserFieldsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('user_fields', function(Blueprint $table)
		{
			$table->foreign('user_id', 'lnk_users_user_fields')->references('id')->on('users')->onUpdate('CASCADE')->onDelete('CASCADE');
			$table->foreign('user_definition_field_id', 'lnk_user_definition_fields_user_fields')->references('id')->on('user_definition_fields')->onUpdate('CASCADE')->onDelete('CASCADE');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('user_fields', function(Blueprint $table)
		{
			$table->dropForeign('lnk_users_user_fields');
			$table->dropForeign('lnk_user_definition_fields_user_fields');
		});
	}

}
