<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToFieldDefinitionsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('field_definitions', function(Blueprint $table)
		{
			$table->foreign('field_type_id', 'lnk_field_types_field_definitions')->references('id')->on('field_types')->onUpdate('CASCADE')->onDelete('CASCADE');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('field_definitions', function(Blueprint $table)
		{
			$table->dropForeign('lnk_field_types_field_definitions');
		});
	}

}
