<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToTeamBoardColumnTasksTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('team_board_column_tasks', function(Blueprint $table)
		{
			$table->foreign('team_board_column_id', 'lnk_team_board_columns_team_board_column_tasks')->references('id')->on('team_board_columns')->onUpdate('CASCADE')->onDelete('CASCADE');
			$table->foreign('task_id', 'lnk_tasks_team_board_column_tasks')->references('id')->on('tasks')->onUpdate('CASCADE')->onDelete('CASCADE');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('team_board_column_tasks', function(Blueprint $table)
		{
			$table->dropForeign('lnk_team_board_columns_team_board_column_tasks');
			$table->dropForeign('lnk_tasks_team_board_column_tasks');
		});
	}

}
