<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToTaskDocumentsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('task_documents', function(Blueprint $table)
		{
			$table->foreign('task_id', 'lnk_tasks_task_documents')->references('id')->on('tasks')->onUpdate('CASCADE')->onDelete('CASCADE');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('task_documents', function(Blueprint $table)
		{
			$table->dropForeign('lnk_tasks_task_documents');
		});
	}

}
