<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateFieldDefinitionsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('field_definitions', function(Blueprint $table)
		{
			$table->smallInteger('id', true);
			$table->smallInteger('field_type_id');
			$table->string('name');
			$table->string('description', 4000)->nullable();
			$table->string('default_value')->nullable();
			$table->string('label');
			$table->smallInteger('length');
			$table->smallInteger('precision')->nullable();
			$table->string('list_of_values_sql', 4000)->nullable();
			$table->boolean('active')->nullable()->default(1);
			$table->text('list_of_values')->nullable();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('field_definitions');
	}

}
